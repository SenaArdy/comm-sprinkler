// Generated code from Butter Knife. Do not modify!
package com.ptfi.commsprinkler;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class MainMenu$$ViewInjector {
  public static void inject(Finder finder, final com.ptfi.commsprinkler.MainMenu target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689840, "field 'toolbar'");
    target.toolbar = (android.support.v7.widget.Toolbar) view;
  }

  public static void reset(com.ptfi.commsprinkler.MainMenu target) {
    target.toolbar = null;
  }
}
