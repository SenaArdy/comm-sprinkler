// Generated code from Butter Knife. Do not modify!
package com.ptfi.commsprinkler;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class PagerSlidingMenu$$ViewInjector {
  public static void inject(Finder finder, final com.ptfi.commsprinkler.PagerSlidingMenu target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689840, "field 'toolbar'");
    target.toolbar = (android.support.v7.widget.Toolbar) view;
    view = finder.findRequiredView(source, 2131689748, "field 'tabs'");
    target.tabs = (com.astuetz.PagerSlidingTabStrip) view;
    view = finder.findRequiredView(source, 2131689749, "field 'pager'");
    target.pager = (android.support.v4.view.ViewPager) view;
  }

  public static void reset(com.ptfi.commsprinkler.PagerSlidingMenu target) {
    target.toolbar = null;
    target.tabs = null;
    target.pager = null;
  }
}
