-keep class android.support.multidex.MultiDexApplication {
    <init>();
    void attachBaseContext(android.content.Context);
}
-keep class com.ptfi.commsprinkler.FirstSplashScreen { <init>(); }
-keep class com.ptfi.commsprinkler.SplashScreen { <init>(); }
-keep class com.ptfi.commsprinkler.MainMenu { <init>(); }
-keep class com.ptfi.commsprinkler.PagerSlidingMenu { <init>(); }
-keep class com.google.zxing.client.android.CaptureActivity { <init>(); }
-keep class br.com.thinkti.android.filechooser.FileChooser { <init>(); }
-keep public class * extends android.app.backup.BackupAgent {
    <init>();
}
-keep public class * extends java.lang.annotation.Annotation {
    *;
}
-keep class com.android.tools.fd.** {
    *;
}
-dontnote com.android.tools.fd.**,android.support.multidex.MultiDexExtractor
