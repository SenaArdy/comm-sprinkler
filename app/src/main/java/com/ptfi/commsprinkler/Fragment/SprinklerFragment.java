package com.ptfi.commsprinkler.Fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.client.android.CaptureActivity;
import com.ptfi.commsprinkler.Adapter.ListComplianceAdapter;
import com.ptfi.commsprinkler.Adapter.ListPhotoAdapter;
import com.ptfi.commsprinkler.Database.DataSource;
import com.ptfi.commsprinkler.Models.ComplianceModel;
import com.ptfi.commsprinkler.Models.DataSingleton;
import com.ptfi.commsprinkler.Models.HandoverModel;
import com.ptfi.commsprinkler.Models.InspectorModel;
import com.ptfi.commsprinkler.Models.PhotoModel;
import com.ptfi.commsprinkler.Models.SprinklerModel;
import com.ptfi.commsprinkler.PopUp.PhotoPopUp;
import com.ptfi.commsprinkler.PopUp.PopUp;
import com.ptfi.commsprinkler.PopUp.PopUpCallbackInspector;
import com.ptfi.commsprinkler.PopUp.PopUpComplianceCallback;
import com.ptfi.commsprinkler.PopUp.PopUpPhotosCallback;
import com.ptfi.commsprinkler.R;
import com.ptfi.commsprinkler.Utils.FoldersFilesName;
import com.ptfi.commsprinkler.Utils.Helper;
import com.ptfi.commsprinkler.Utils.LanguageConstants;
import com.ptfi.commsprinkler.Utils.Reports;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;
import org.honorato.multistatetogglebutton.ToggleButton;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by senaardyputra on 6/16/16.
 */
public class SprinklerFragment extends Fragment {

    protected static FragmentActivity mActivity;

    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    static final int REQUEST_SCAN_BARCODE_CLIENT = 347;
    static final int REQUEST_SCAN_BARCODE_ENG = 348;
    static final int REQUEST_SCAN_BARCODE_MAIN = 349;
    static final int REQUEST_SCAN_BARCODE_AO = 350;
    static final int REQUEST_SCAN_BARCODE_MAINRES = 351;
    static final int REQUEST_SCAN_BARCODE_CSE = 352;
    static final int REQUEST_SCAN_BARCODE_DEPT = 353;

    public PhotoModel activeFModel = null;

    private Dialog activeDialog;

    static String inspectorIDFalse;

    ArrayList<InspectorModel> dataInspector;

    static EditText equipmentET, dateET, clientET, typeET, registerET, locationET, remark1ET, remark2ET, remark3ET, remark4ET, remark5ET, remark6ET, remark7ET, remark8ET,
            remark9ET, remark10ET, remark11ET, remark12ET, remark13ET, remark14ET, remark15ET, remark16ET, remark17ET, remark18ET, engineeringET, maintenanceET,
            areaOwnerET, ugmrET, cseET, deptHeadET;

    static TextInputLayout equipmentTL, dateTL, clientTL, typeTL, registerTL, locationTL, remark1TL, remark2TL, remark3TL, remark4TL, remark5TL, remark6TL, remark7TL, remark8TL,
            remark9TL, remark10TL, remark11TL, remark12TL, remark13TL, remark14TL, remark15TL, remark16TL, remark17TL, remark18TL, engineeringTL, maintenanceTL,
            areaOwnerTL, ugmrTL, cseTL, deptHeadTL;

    static MultiStateToggleButton insp1, insp2, insp3, insp4, insp5, insp6, insp7, insp8, insp9, insp10, insp11, insp12, insp13,
            insp14, insp15, insp16, insp17, insp18;

    static TextView Q1TV, Q2TV, Q3TV, Q4TV, Q5TV, Q6TV, Q7TV, Q8TV, Q9TV, Q10TV, Q11TV, Q12TV, Q13TV, Q14TV, Q15TV, Q16TV,
            Q17TV, Q18TV, Q60TV, Q70TV, Q80TV, Q90TV, Q100TV;

    static ImageView addComplianceIV, addPhotosIV;

    static LinearLayout findingsLL, photosLL;

    static RecyclerView findingsRV, photosRV;

    TextView imeiCode, versionCode;

    static CardView generateCV, previewCV;

    private PopUp.page pages;
    private PopUp.signature signatures;

    static RecyclerView.LayoutManager layoutManager;
    RecyclerView.LayoutManager photosLayoutManager;

    private static ArrayList<ComplianceModel> complianceData = new ArrayList<>();
    private static ArrayList<PhotoModel> photoData = new ArrayList<>();

    static ListComplianceAdapter adapterData;
    ListPhotoAdapter adapterPhotosData;

    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;

    LanguageConstants languageConstants;
    android.widget.ToggleButton languageToggleButton;
    MultiStateToggleButton[] compilanceBtns;
    int[] compilanceLastValue;
    boolean isChangedLanguage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        PopUp.pageComm = PopUp.page.commissioning;

        versionCode = (TextView) rootView.findViewById(R.id.versionCode);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionCode.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiCode = (TextView) rootView.findViewById(R.id.imeiCode);
        imeiCode.setText("Device IMEI : " + deviceID);

        initComponent(rootView);
        setListener();
        findingRv();
        photosRv();

        return rootView;
    }

    public void initComponent(final View rootView) {
        if (rootView != null) {
            equipmentET = (EditText) rootView.findViewById(R.id.equipmentET);
            registerET = (EditText) rootView.findViewById(R.id.registerET);
            dateET = (EditText) rootView.findViewById(R.id.dateET);
            clientET = (EditText) rootView.findViewById(R.id.clientET);
            typeET = (EditText) rootView.findViewById(R.id.typeEqET);
            locationET = (EditText) rootView.findViewById(R.id.locationEqET);

            equipmentTL = (TextInputLayout) rootView.findViewById(R.id.equipmentTL);
            registerTL = (TextInputLayout) rootView.findViewById(R.id.registerTL);
            dateTL = (TextInputLayout) rootView.findViewById(R.id.dateTL);
            clientTL = (TextInputLayout) rootView.findViewById(R.id.clientTL);
            typeTL = (TextInputLayout) rootView.findViewById(R.id.typeEqTL);
            locationTL = (TextInputLayout) rootView.findViewById(R.id.locationEqTL);

            engineeringET = (EditText) rootView.findViewById(R.id.engineeringET);
            maintenanceET = (EditText) rootView.findViewById(R.id.maintenanceET);
            areaOwnerET = (EditText) rootView.findViewById(R.id.areaOwnerET);
            ugmrET = (EditText) rootView.findViewById(R.id.ugmrET);
            cseET = (EditText) rootView.findViewById(R.id.cseET);
            deptHeadET = (EditText) rootView.findViewById(R.id.deptHeadET);

            engineeringTL = (TextInputLayout) rootView.findViewById(R.id.engineeringTL);
            maintenanceTL = (TextInputLayout) rootView.findViewById(R.id.maintenanceTL);
            areaOwnerTL = (TextInputLayout) rootView.findViewById(R.id.areaOwnerTL);
            ugmrTL = (TextInputLayout) rootView.findViewById(R.id.ugmrTL);
            cseTL = (TextInputLayout) rootView.findViewById(R.id.cseTL);
            deptHeadTL = (TextInputLayout) rootView.findViewById(R.id.deptHeadTL);

            remark1ET = (EditText) rootView.findViewById(R.id.remark1ET);
            remark2ET = (EditText) rootView.findViewById(R.id.remark2ET);
            remark3ET = (EditText) rootView.findViewById(R.id.remark3ET);
            remark4ET = (EditText) rootView.findViewById(R.id.remark4ET);
            remark5ET = (EditText) rootView.findViewById(R.id.remark5ET);
            remark6ET = (EditText) rootView.findViewById(R.id.remark6ET);
            remark7ET = (EditText) rootView.findViewById(R.id.remark7ET);
            remark8ET = (EditText) rootView.findViewById(R.id.remark8ET);
            remark9ET = (EditText) rootView.findViewById(R.id.remark9ET);
            remark10ET = (EditText) rootView.findViewById(R.id.remark10ET);
            remark11ET = (EditText) rootView.findViewById(R.id.remark11ET);
            remark12ET = (EditText) rootView.findViewById(R.id.remark12ET);
            remark13ET = (EditText) rootView.findViewById(R.id.remark13ET);
            remark14ET = (EditText) rootView.findViewById(R.id.remark14ET);
            remark15ET = (EditText) rootView.findViewById(R.id.remark15ET);
            remark16ET = (EditText) rootView.findViewById(R.id.remark16ET);
            remark17ET = (EditText) rootView.findViewById(R.id.remark17ET);
            remark18ET = (EditText) rootView.findViewById(R.id.remark18ET);

            remark1TL = (TextInputLayout) rootView.findViewById(R.id.remark1TL);
            remark2TL = (TextInputLayout) rootView.findViewById(R.id.remark2TL);
            remark3TL = (TextInputLayout) rootView.findViewById(R.id.remark3TL);
            remark4TL = (TextInputLayout) rootView.findViewById(R.id.remark4TL);
            remark5TL = (TextInputLayout) rootView.findViewById(R.id.remark5TL);
            remark6TL = (TextInputLayout) rootView.findViewById(R.id.remark6TL);
            remark7TL = (TextInputLayout) rootView.findViewById(R.id.remark7TL);
            remark8TL = (TextInputLayout) rootView.findViewById(R.id.remark8TL);
            remark9TL = (TextInputLayout) rootView.findViewById(R.id.remark9TL);
            remark10TL = (TextInputLayout) rootView.findViewById(R.id.remark10TL);
            remark11TL = (TextInputLayout) rootView.findViewById(R.id.remark11TL);
            remark12TL = (TextInputLayout) rootView.findViewById(R.id.remark12TL);
            remark13TL = (TextInputLayout) rootView.findViewById(R.id.remark13TL);
            remark14TL = (TextInputLayout) rootView.findViewById(R.id.remark14TL);
            remark15TL = (TextInputLayout) rootView.findViewById(R.id.remark15TL);
            remark16TL = (TextInputLayout) rootView.findViewById(R.id.remark16TL);
            remark17TL = (TextInputLayout) rootView.findViewById(R.id.remark17TL);
            remark18TL = (TextInputLayout) rootView.findViewById(R.id.remark18TL);

            insp1 = (MultiStateToggleButton) rootView.findViewById(R.id.insp1);
            insp2 = (MultiStateToggleButton) rootView.findViewById(R.id.insp2);
            insp3 = (MultiStateToggleButton) rootView.findViewById(R.id.insp3);
            insp4 = (MultiStateToggleButton) rootView.findViewById(R.id.insp4);
            insp5 = (MultiStateToggleButton) rootView.findViewById(R.id.insp5);
            insp6 = (MultiStateToggleButton) rootView.findViewById(R.id.insp6);
            insp7 = (MultiStateToggleButton) rootView.findViewById(R.id.insp7);
            insp8 = (MultiStateToggleButton) rootView.findViewById(R.id.insp8);
            insp9 = (MultiStateToggleButton) rootView.findViewById(R.id.insp9);
            insp10 = (MultiStateToggleButton) rootView.findViewById(R.id.insp10);
            insp11 = (MultiStateToggleButton) rootView.findViewById(R.id.insp11);
            insp12 = (MultiStateToggleButton) rootView.findViewById(R.id.insp12);
            insp13 = (MultiStateToggleButton) rootView.findViewById(R.id.insp13);
            insp14 = (MultiStateToggleButton) rootView.findViewById(R.id.insp14);
            insp15 = (MultiStateToggleButton) rootView.findViewById(R.id.insp15);
            insp16 = (MultiStateToggleButton) rootView.findViewById(R.id.insp16);
            insp17 = (MultiStateToggleButton) rootView.findViewById(R.id.insp17);
            insp18 = (MultiStateToggleButton) rootView.findViewById(R.id.insp18);

            Q1TV = (TextView) rootView.findViewById(R.id.Q1TV);
            Q2TV = (TextView) rootView.findViewById(R.id.Q2TV);
            Q3TV = (TextView) rootView.findViewById(R.id.Q3TV);
            Q4TV = (TextView) rootView.findViewById(R.id.Q4TV);
            Q5TV = (TextView) rootView.findViewById(R.id.Q5TV);
            Q6TV = (TextView) rootView.findViewById(R.id.Q6TV);
            Q7TV = (TextView) rootView.findViewById(R.id.Q7TV);
            Q8TV = (TextView) rootView.findViewById(R.id.Q8TV);
            Q9TV = (TextView) rootView.findViewById(R.id.Q9TV);
            Q10TV = (TextView) rootView.findViewById(R.id.Q10TV);
            Q11TV = (TextView) rootView.findViewById(R.id.Q11TV);
            Q12TV = (TextView) rootView.findViewById(R.id.Q12TV);
            Q13TV = (TextView) rootView.findViewById(R.id.Q13TV);
            Q14TV = (TextView) rootView.findViewById(R.id.Q14TV);
            Q15TV = (TextView) rootView.findViewById(R.id.Q15TV);
            Q16TV = (TextView) rootView.findViewById(R.id.Q16TV);
            Q17TV = (TextView) rootView.findViewById(R.id.Q17TV);
            Q18TV = (TextView) rootView.findViewById(R.id.Q18TV);
            Q60TV = (TextView) rootView.findViewById(R.id.Q60TV);
            Q70TV = (TextView) rootView.findViewById(R.id.Q70TV);
            Q80TV = (TextView) rootView.findViewById(R.id.Q80TV);
            Q90TV = (TextView) rootView.findViewById(R.id.Q90TV);
            Q100TV = (TextView) rootView.findViewById(R.id.Q100TV);

            activeFModel = new PhotoModel();

            addComplianceIV = (ImageView) rootView.findViewById(R.id.addComplianceIV);
            addComplianceIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopUp.showCompliance(mActivity, equipmentET.getText().toString(), dateET.getText().toString(), registerET.getText().toString(),
                            null, null, null, null, true, new PopUpComplianceCallback() {
                                @Override
                                public void onDataSave(ComplianceModel model) {
                                    DataSource ds = new DataSource(mActivity);
                                    ds.open();

                                    ds.insertCompliance(model);
                                    findingRv();

                                    ds.close();
                                }
                            });
                }
            });

            addPhotosIV = (ImageView) rootView.findViewById(R.id.addPhotosIV);

            addPhotosIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PhotoPopUp.showAddImagePopUp(mActivity, equipmentET.getText().toString(),
                            dateET.getText().toString(), registerET.getText().toString(), new PopUpPhotosCallback() {
                                @Override
                                public void onDataSave(PhotoModel model) {
                                    DataSource ds = new DataSource(mActivity);
                                    ds.open();

                                    ds.insertPhotos(model);
                                    photosRv();

                                    ds.close();
                                }
                            });
                }
            });

            addPhotosIV = (ImageView) rootView.findViewById(R.id.addPhotosIV);

            findingsLL = (LinearLayout) rootView.findViewById(R.id.findingsLL);
            photosLL = (LinearLayout) rootView.findViewById(R.id.photosLL);

            findingsRV = (RecyclerView) rootView.findViewById(R.id.findingsRV);
            photosRV = (RecyclerView) rootView.findViewById(R.id.photosRV);

            generateCV = (CardView) rootView.findViewById(R.id.generateCV);
            previewCV = (CardView) rootView.findViewById(R.id.previewCV);

            generateCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validationGenerate()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            Reports.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), true);
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            Reports.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), true);
                        }
                        ds.close();
                    }
                }
            });

            previewCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validationGenerate()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            Reports.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), false);
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            Reports.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), false);
                        }
                        ds.close();
                    }
                }
            });

            Q80TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(findingsRV.getVisibility() == View.GONE) {
                        findingsRV.setVisibility(View.VISIBLE);
                    } else if ( findingsRV.getVisibility() == View.VISIBLE) {
                        findingsRV.setVisibility(View.GONE);
                    }
                }
            });

            Q90TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(photosRV.getVisibility() == View.GONE) {
                        photosRV.setVisibility(View.VISIBLE);
                    } else if ( photosRV.getVisibility() == View.VISIBLE) {
                        photosRV.setVisibility(View.GONE);
                    }
                }
            });

            engineeringET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            signatures = PopUp.signature.engineering;
                            scanBar(signatures);
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            signatures = PopUp.signature.engineering;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            maintenanceET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            signatures = PopUp.signature.maintenance;
                            scanBar(signatures);
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            signatures = PopUp.signature.maintenance;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            areaOwnerET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        final DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.areaOwner;
                            PopUp.showInspectorPopUp(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {
                                            ds.updateAOCommissioning(model);
                                            areaOwnerET.setText(name);
                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.areaOwner;
                            PopUp.showInspectorPopUp(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {
                                            ds.updateAOCommissioning(model);
                                            areaOwnerET.setText(name);
                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            ugmrET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        final  DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.mainRes;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {
                                            ds.updateMainResCommissioning(model);
                                            ugmrTL.setHint(division);
                                            ugmrET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.mainRes;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {
                                            ds.updateMainResCommissioning(model);
                                            ugmrTL.setHint(division);
                                            ugmrET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            cseET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        final DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.CSE;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {
                                            ds.updateCSECommissioning(model);
                                            cseTL.setHint(division);
                                            cseET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });

                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.CSE;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {
                                            ds.updateCSECommissioning(model);
                                            cseTL.setHint(division);
                                            cseET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            deptHeadET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            signatures = PopUp.signature.deptHead;
                            scanBar(signatures);
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            signatures = PopUp.signature.deptHead;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            final Calendar c = Calendar.getInstance();
            selectedYear = c.get(Calendar.YEAR);
            selectedMonth = c.get(Calendar.MONTH);
            selectedDate = c.get(Calendar.DAY_OF_MONTH);
            DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                    selectedDate);
            dateET.setText(DataSingleton.getInstance().getFormattedDate());

            languageToggleButton = (android.widget.ToggleButton) rootView.findViewById(R.id.toggleBtnLanguange);

            compilanceBtns = new MultiStateToggleButton[]{insp1, insp2, insp3, insp4, insp5, insp6, insp7, insp8, insp9, insp10, insp11,
                    insp12, insp13, insp14, insp15, insp16, insp17, insp18};

            compilanceLastValue = new int[]{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                    -1, -1, -1, -1, -1, -1};

            isChangedLanguage = false;

        }
    }

    public static boolean validation() {
        boolean status = true;
        String emptyFields = "";

        if (equipmentET.getText().toString().length() == 0) {
            status = false;
            equipmentTL.setErrorEnabled(true);
            equipmentTL.setError("*Please fill Sprinkler Equipment");
            emptyFields += "Please fill Sprinkler Equipment\n";
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill Date Inspection");
            emptyFields += "Please fill Date Inspection\n";
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill Sprinkler Location");
            emptyFields += "Please fill Sprinkler Location\n";
        }

        if (typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("*Please fill Sprinkler Type");
            emptyFields += "Please fill Sprinkler Type\n";
        }

        if (clientET.getText().toString().length() == 0) {
            status = false;
            clientTL.setErrorEnabled(true);
            clientTL.setError("*Please fill Client Data");
            emptyFields += "Please fill Client Data\n";
        }

//        if (registerET.getText().toString().length() == 0) {
//            status = false;
//            registerTL.setErrorEnabled(true);
//            registerTL.setError("*Please fill Sprinkler Register");
//        }

        if (insp1.getValue() == 1){
            if (remark1ET.getText().toString().length() == 0) {
                status = false;
                remark1TL.setErrorEnabled(true);
                remark1TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.1\n";
            }
        } else if (insp1.getValue() == -1) {
            status = false;
            remark1TL.setErrorEnabled(true);
            remark1TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.1\n";
        }

        if (insp2.getValue() == 1){
            if (remark2ET.getText().toString().length() == 0) {
                status = false;
                remark2TL.setErrorEnabled(true);
                remark2TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.2\n";
            }
        } else if (insp2.getValue() == -1) {
            status = false;
            remark2TL.setErrorEnabled(true);
            remark2TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.2\n";
        }

        if (insp3.getValue() == 1){
            if (remark3ET.getText().toString().length() == 0) {
                status = false;
                remark3TL.setErrorEnabled(true);
                remark3TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.3\n";
            }
        } else if (insp3.getValue() == -1) {
            status = false;
            remark3TL.setErrorEnabled(true);
            remark3TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.3\n";
        }

        if (insp4.getValue() == 1){
            if (remark4ET.getText().toString().length() == 0) {
                status = false;
                remark4TL.setErrorEnabled(true);
                remark4TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.4\n";
            }
        } else if (insp4.getValue() == -1) {
            status = false;
            remark4TL.setErrorEnabled(true);
            remark4TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.4\n";
        }

        if (insp5.getValue() == 1){
            if (remark5ET.getText().toString().length() == 0) {
                status = false;
                remark5TL.setErrorEnabled(true);
                remark5TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.5\n";
            }
        } else if (insp5.getValue() == -1) {
            status = false;
            remark5TL.setErrorEnabled(true);
            remark5TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.5\n";
        }

        if (insp6.getValue() == 1){
            if (remark6ET.getText().toString().length() == 0) {
                status = false;
                remark6TL.setErrorEnabled(true);
                remark6TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.6\n";
            }
        } else if (insp6.getValue() == -1) {
            status = false;
            remark6TL.setErrorEnabled(true);
            remark6TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.6\n";
        }

        if (insp7.getValue() == 1){
            if (remark7ET.getText().toString().length() == 0) {
                status = false;
                remark7TL.setErrorEnabled(true);
                remark7TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.7\n";
            }
        } else if (insp7.getValue() == -1) {
            status = false;
            remark7TL.setErrorEnabled(true);
            remark7TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.7\n";
        }

        if (insp8.getValue() == 1){
            if (remark8ET.getText().toString().length() == 0) {
                status = false;
                remark8TL.setErrorEnabled(true);
                remark8TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.8\n";
            }
        } else if (insp8.getValue() == -1) {
            status = false;
            remark8TL.setErrorEnabled(true);
            remark8TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.8\n";
        }

        if (insp9.getValue() == 1){
            if (remark9ET.getText().toString().length() == 0) {
                status = false;
                remark9TL.setErrorEnabled(true);
                remark9TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.9\n";
            }
        } else if (insp9.getValue() == -1) {
            status = false;
            remark9TL.setErrorEnabled(true);
            remark9TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.9\n";
        }

        if (insp10.getValue() == 1){
            if (remark10ET.getText().toString().length() == 0) {
                status = false;
                remark10TL.setErrorEnabled(true);
                remark10TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.10\n";
            }
        } else if (insp10.getValue() == -1) {
            status = false;
            remark10TL.setErrorEnabled(true);
            remark10TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.10\n";
        }

        if (insp11.getValue() == 1){
            if (remark11ET.getText().toString().length() == 0) {
                status = false;
                remark11TL.setErrorEnabled(true);
                remark11TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.11\n";
            }
        } else if (insp11.getValue() == -1) {
            status = false;
            remark11TL.setErrorEnabled(true);
            remark11TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.11\n";
        }

        if (insp12.getValue() == 1){
            if (remark12ET.getText().toString().length() == 0) {
                status = false;
                remark12TL.setErrorEnabled(true);
                remark12TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.12\n";
            }
        } else if (insp12.getValue() == -1) {
            status = false;
            remark12TL.setErrorEnabled(true);
            remark12TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.12\n";
        }

        if (insp13.getValue() == 1){
            if (remark13ET.getText().toString().length() == 0) {
                status = false;
                remark13TL.setErrorEnabled(true);
                remark13TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.13\n";
            }
        } else if (insp13.getValue() == -1) {
            status = false;
            remark13TL.setErrorEnabled(true);
            remark13TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.13\n";
        }

        if (insp14.getValue() == 1){
            if (remark14ET.getText().toString().length() == 0) {
                status = false;
                remark14TL.setErrorEnabled(true);
                remark14TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.14\n";
            }
        } else if (insp14.getValue() == -1) {
            status = false;
            remark14TL.setErrorEnabled(true);
            remark14TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.14\n";
        }

        if (insp15.getValue() == 1){
            if (remark15ET.getText().toString().length() == 0) {
                status = false;
                remark15TL.setErrorEnabled(true);
                remark15TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.15\n";
            }
        } else if (insp15.getValue() == -1) {
            status = false;
            remark15TL.setErrorEnabled(true);
            remark15TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.15\n";
        }

        if (insp16.getValue() == 1){
            if (remark16ET.getText().toString().length() == 0) {
                status = false;
                remark16TL.setErrorEnabled(true);
                remark16TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.16\n";
            }
        } else if (insp16.getValue() == -1) {
            status = false;
            remark16TL.setErrorEnabled(true);
            remark16TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.16\n";
        }

        if (insp17.getValue() == 1){
            if (remark17ET.getText().toString().length() == 0) {
                status = false;
                remark17TL.setErrorEnabled(true);
                remark17TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.17\n";
            }
        } else if (insp17.getValue() == -1) {
            status = false;
            remark17TL.setErrorEnabled(true);
            remark17TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.17\n";
        }

        if (insp18.getValue() == 1){
            if (remark18ET.getText().toString().length() == 0) {
                status = false;
                remark18TL.setErrorEnabled(true);
                remark18TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.18\n";
            }
        } else if (insp18.getValue() == -1) {
            status = false;
            remark18TL.setErrorEnabled(true);
            remark18TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.18\n";
        }

        if (!status) {
            Helper.showPopUpMessage(mActivity, "Warning",
                    "Please fill the empty field(s):\n" + emptyFields, null);
        }

        return status;
    }

    public static boolean validationClient() {
        boolean status = true;

        if (equipmentET.getText().toString().length() == 0) {
            status = false;
            equipmentTL.setErrorEnabled(true);
            equipmentTL.setError("*Please fill Sprinkler Equipment");
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill Date Inspection");
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill Sprinkler Location");
        }

        if (typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("*Please fill Sprinkler Type");
        }

//        if (registerET.getText().toString().length() == 0) {
//            status = false;
//            registerTL.setErrorEnabled(true);
//            registerTL.setError("*Please fill Sprinkler Register");
//        }

        return status;
    }

    public static boolean validationGenerate() {
        boolean status = true;
        String emptyFields = "";

        if (equipmentET.getText().toString().length() == 0) {
            status = false;
            equipmentTL.setErrorEnabled(true);
            equipmentTL.setError("*Please fill Sprinkler Equipment");
            emptyFields += "Please fill Sprinkler Equipment\n";
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill Date Inspection");
            emptyFields += "Please fill Date Inspection\n";
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill Sprinkler Location");
            emptyFields += "Please fill Sprinkler Location\n";
        }

        if (typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("*Please fill Sprinkler Type");
            emptyFields += "Please fill Sprinkler Type\n";
        }

        if (clientET.getText().toString().length() == 0) {
            status = false;
            clientTL.setErrorEnabled(true);
            clientTL.setError("*Please fill Client Data");
            emptyFields += "Please fill Client Data\n";
        }

        if (engineeringET.getText().toString().length() == 0) {
            status = false;
            engineeringTL.setErrorEnabled(true);
            engineeringTL.setError("*Please fill Engineering Name");
            emptyFields += "Please fill Engineering Name\n";
        }

//        if (maintenanceET.getText().toString().length() == 0) {
//            status = false;
//            maintenanceTL.setErrorEnabled(true);
//            maintenanceTL.setError("*Please fill Maintenance Name");
//            emptyFields += "Please fill Maintenance Name\n";
//        }
//
//        if (areaOwnerET.getText().toString().length() == 0) {
//            status = false;
//            areaOwnerTL.setErrorEnabled(true);
//            areaOwnerTL.setError("*Please fill Area Owner Name");
//            emptyFields += "Please fill Area Owner Name\n";
//        }
//
//        if (ugmrET.getText().toString().length() == 0) {
//            status = false;
//            ugmrTL.setErrorEnabled(true);
//            ugmrTL.setError("*Please fill UGMR Name");
//            emptyFields += "Please fill UGMR Name\n";
//        }
//
//        if (cseET.getText().toString().length() == 0) {
//            status = false;
//            cseTL.setErrorEnabled(true);
//            cseTL.setError("*Please fill CSE Name");
//            emptyFields += "Please fill CSE Name\n";
//        }
//
//        if (deptHeadET.getText().toString().length() == 0) {
//            status = false;
//            deptHeadTL.setErrorEnabled(true);
//            deptHeadTL.setError("*Please fill Department Head Name");
//            emptyFields += "Please fill Department Head Name\n";
//        }

        if (insp1.getValue() == 1){
            if (remark1ET.getText().toString().length() == 0) {
                status = false;
                remark1TL.setErrorEnabled(true);
                remark1TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.1\n";
            }
        } else if (insp1.getValue() == -1) {
            status = false;
            remark1TL.setErrorEnabled(true);
            remark1TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.1\n";
        }

        if (insp2.getValue() == 1){
            if (remark2ET.getText().toString().length() == 0) {
                status = false;
                remark2TL.setErrorEnabled(true);
                remark2TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.2\n";
            }
        } else if (insp2.getValue() == -1) {
            status = false;
            remark2TL.setErrorEnabled(true);
            remark2TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.2\n";
        }

        if (insp3.getValue() == 1){
            if (remark3ET.getText().toString().length() == 0) {
                status = false;
                remark3TL.setErrorEnabled(true);
                remark3TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.3\n";
            }
        } else if (insp3.getValue() == -1) {
            status = false;
            remark3TL.setErrorEnabled(true);
            remark3TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.3\n";
        }

        if (insp4.getValue() == 1){
            if (remark4ET.getText().toString().length() == 0) {
                status = false;
                remark4TL.setErrorEnabled(true);
                remark4TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.4\n";
            }
        } else if (insp4.getValue() == -1) {
            status = false;
            remark4TL.setErrorEnabled(true);
            remark4TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.4\n";
        }

        if (insp5.getValue() == 1){
            if (remark5ET.getText().toString().length() == 0) {
                status = false;
                remark5TL.setErrorEnabled(true);
                remark5TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.5\n";
            }
        } else if (insp5.getValue() == -1) {
            status = false;
            remark5TL.setErrorEnabled(true);
            remark5TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.5\n";
        }

        if (insp6.getValue() == 1){
            if (remark6ET.getText().toString().length() == 0) {
                status = false;
                remark6TL.setErrorEnabled(true);
                remark6TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.6\n";
            }
        } else if (insp6.getValue() == -1) {
            status = false;
            remark6TL.setErrorEnabled(true);
            remark6TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.6\n";
        }

        if (insp7.getValue() == 1){
            if (remark7ET.getText().toString().length() == 0) {
                status = false;
                remark7TL.setErrorEnabled(true);
                remark7TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.7\n";
            }
        } else if (insp7.getValue() == -1) {
            status = false;
            remark7TL.setErrorEnabled(true);
            remark7TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.7\n";
        }

        if (insp8.getValue() == 1){
            if (remark8ET.getText().toString().length() == 0) {
                status = false;
                remark8TL.setErrorEnabled(true);
                remark8TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.8\n";
            }
        } else if (insp8.getValue() == -1) {
            status = false;
            remark8TL.setErrorEnabled(true);
            remark8TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.8\n";
        }

        if (insp9.getValue() == 1){
            if (remark9ET.getText().toString().length() == 0) {
                status = false;
                remark9TL.setErrorEnabled(true);
                remark9TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.9\n";
            }
        } else if (insp9.getValue() == -1) {
            status = false;
            remark9TL.setErrorEnabled(true);
            remark9TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.9\n";
        }

        if (insp10.getValue() == 1){
            if (remark10ET.getText().toString().length() == 0) {
                status = false;
                remark10TL.setErrorEnabled(true);
                remark10TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.10\n";
            }
        } else if (insp10.getValue() == -1) {
            status = false;
            remark10TL.setErrorEnabled(true);
            remark10TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.10\n";
        }

        if (insp11.getValue() == 1){
            if (remark11ET.getText().toString().length() == 0) {
                status = false;
                remark11TL.setErrorEnabled(true);
                remark11TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.11\n";
            }
        } else if (insp11.getValue() == -1) {
            status = false;
            remark11TL.setErrorEnabled(true);
            remark11TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.11\n";
        }

        if (insp12.getValue() == 1){
            if (remark12ET.getText().toString().length() == 0) {
                status = false;
                remark12TL.setErrorEnabled(true);
                remark12TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.12\n";
            }
        } else if (insp12.getValue() == -1) {
            status = false;
            remark12TL.setErrorEnabled(true);
            remark12TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.12\n";
        }

        if (insp13.getValue() == 1){
            if (remark13ET.getText().toString().length() == 0) {
                status = false;
                remark13TL.setErrorEnabled(true);
                remark13TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.13\n";
            }
        } else if (insp13.getValue() == -1) {
            status = false;
            remark13TL.setErrorEnabled(true);
            remark13TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.13\n";
        }

        if (insp14.getValue() == 1){
            if (remark14ET.getText().toString().length() == 0) {
                status = false;
                remark14TL.setErrorEnabled(true);
                remark14TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.14\n";
            }
        } else if (insp14.getValue() == -1) {
            status = false;
            remark14TL.setErrorEnabled(true);
            remark14TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.14\n";
        }

        if (insp15.getValue() == 1){
            if (remark15ET.getText().toString().length() == 0) {
                status = false;
                remark15TL.setErrorEnabled(true);
                remark15TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.15\n";
            }
        } else if (insp15.getValue() == -1) {
            status = false;
            remark15TL.setErrorEnabled(true);
            remark15TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.15\n";
        }

        if (insp16.getValue() == 1){
            if (remark16ET.getText().toString().length() == 0) {
                status = false;
                remark16TL.setErrorEnabled(true);
                remark16TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.16\n";
            }
        } else if (insp16.getValue() == -1) {
            status = false;
            remark16TL.setErrorEnabled(true);
            remark16TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.16\n";
        }

        if (insp17.getValue() == 1){
            if (remark17ET.getText().toString().length() == 0) {
                status = false;
                remark17TL.setErrorEnabled(true);
                remark17TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.17\n";
            }
        } else if (insp17.getValue() == -1) {
            status = false;
            remark17TL.setErrorEnabled(true);
            remark17TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.17\n";
        }

        if (insp18.getValue() == 1){
            if (remark18ET.getText().toString().length() == 0) {
                status = false;
                remark18TL.setErrorEnabled(true);
                remark18TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.18\n";
            }
        } else if (insp18.getValue() == -1) {
            status = false;
            remark18TL.setErrorEnabled(true);
            remark18TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.18\n";
        }

        if (!status) {
            Helper.showPopUpMessage(mActivity, "Warning",
                    "Please fill the empty field(s):\n" + emptyFields, null);
        }

        return status;
    }

    public void setListener() {
        languageToggleButton
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        String language = "";
                        if (isChecked) {
                            language = "English Language";
                        } else {
                            language = "Bahasa Indonesia";
                        }
                        setLanguage(isChecked);
                        Toast.makeText(mActivity, language,
                                Toast.LENGTH_SHORT).show();
                    }
                });

        for (int i = 0; i < compilanceBtns.length; i++) {
            setCompilanceListener(compilanceBtns[i], i);
        }
    }

    public void setCompilanceListener(MultiStateToggleButton compilanceBtn,
                                      final int index) {
        compilanceBtn
                .setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {

                    @Override
                    public void onValueChanged(int value) {
                        if (!isChangedLanguage) {
                            compilanceLastValue[index] = value;
                        }
                    }

                });
    }

    public void setLanguage(boolean isEnglish) {
        if (isEnglish) {
            languageConstants = new LanguageConstants(false);
        } else {
            languageConstants = new LanguageConstants(true);
        }

        Q1TV.setText(languageConstants.Q1);
        Q2TV.setText(languageConstants.Q2);
        Q3TV.setText(languageConstants.Q3);
        Q4TV.setText(languageConstants.Q4);
        Q5TV.setText(languageConstants.Q5);
        Q6TV.setText(languageConstants.Q6);
        Q7TV.setText(languageConstants.Q7);
        Q8TV.setText(languageConstants.Q8);
        Q9TV.setText(languageConstants.Q9);
        Q10TV.setText(languageConstants.Q10);
        Q11TV.setText(languageConstants.Q11);
        Q12TV.setText(languageConstants.Q12);
        Q13TV.setText(languageConstants.Q13);
        Q14TV.setText(languageConstants.Q14);
        Q15TV.setText(languageConstants.Q15);
        Q16TV.setText(languageConstants.Q16);
        Q17TV.setText(languageConstants.Q17);
        Q18TV.setText(languageConstants.Q18);

        isChangedLanguage = true;

        for (MultiStateToggleButton compilanceBtn : compilanceBtns) {
            compilanceBtn.setValue(-1);
            // With an array
            CharSequence[] textEn = new CharSequence[]{"Pass", "No", "N/A"};
            CharSequence[] textId = new CharSequence[]{"Ya", "Tidak", "N/A"};
            if (isEnglish)
                compilanceBtn.setElements(textId);
            else
                compilanceBtn.setElements(textEn);
        }

        isChangedLanguage = false;

        int index = 0;
        for (MultiStateToggleButton compilanceBtn : compilanceBtns) {
            compilanceBtn.setValue(compilanceLastValue[index]);
            index++;
        }
    }

    public static void findingRv() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        layoutManager = new LinearLayoutManager(mActivity);
        findingsRV.setLayoutManager(layoutManager);
        findingsRV.setItemAnimator(new DefaultItemAnimator());

        complianceData = ds.getAllComplianceEquipment(equipmentET.getText().toString(),
                dateET.getText().toString(), registerET.getText().toString());

        adapterData = new ListComplianceAdapter(mActivity, complianceData);
        findingsRV.setAdapter(adapterData);
        ds.close();
    }

    public void photosRv() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        photosLayoutManager = new LinearLayoutManager(getActivity());
        photosRV.setLayoutManager(photosLayoutManager);
        photosRV.setItemAnimator(new DefaultItemAnimator());

        photoData = ds.getAllPhotoData(equipmentET.getText().toString(),
                dateET.getText().toString(), registerET.getText().toString());

        adapterPhotosData = new ListPhotoAdapter(mActivity, photoData);
        photosRV.setAdapter(adapterPhotosData);
        ds.close();
    }

    public static void datePickers(final Activity mActivity, final View rootView) {
        Helper.showDatePicker(rootView, (FragmentActivity) mActivity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        selectedYear = year;
                        selectedMonth = monthOfYear;
                        selectedDate = dayOfMonth;

                        DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                        ((EditText) rootView).setText(DataSingleton.getInstance()
                                .getFormattedDate());
                    }
                }, selectedYear, selectedMonth, selectedDate);
    }

    private static android.app.AlertDialog showQRDialog(final Activity act,
                                                        CharSequence title, CharSequence message, CharSequence buttonYes,
                                                        CharSequence buttonNo) {
        android.app.AlertDialog.Builder downloadDialog = new android.app.AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Helper.copyAndOpenBarcodeScannerAPK(act);
                    }
                });
        downloadDialog.setNegativeButton(buttonNo,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        return downloadDialog.show();
    }

    public static void scanBar(PopUp.signature signatures) {
        try {
            Intent intent = new Intent(mActivity, CaptureActivity.class);
            intent.setAction(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "ONE_D_MODE");
            switch (signatures) {
                case engineering:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_ENG);
                    break;

                case maintenance:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAIN);
                    break;

//                case areaOwner:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_AO);
//                    break;
//
//                case mainRes:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAINRES);
//                    break;
//
//                case CSE:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_CSE);
//                    break;

                case deptHead:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_DEPT);
                    break;

//                case client:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_CLIENT);
//                    break;

                default:
                    break;
            }
        } catch (ActivityNotFoundException anfe) {
            showQRDialog(mActivity, "No Scanner Found",
                    "Install a scanner code application?", "Yes", "No").show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        pages = PopUp.page.commissioning;

        if (requestCode == REQUEST_SCAN_BARCODE_ENG
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            engineeringET.setText(ds.getNameFromID(inspectorID));
            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.engineering;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
                        registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_ENG
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

        if (requestCode == REQUEST_SCAN_BARCODE_MAIN
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            maintenanceET.setText(ds.getNameFromID(inspectorID));
            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.maintenance;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
                        registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_MAIN
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

//        if (requestCode == REQUEST_SCAN_BARCODE_AO
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            areaOwnerET.setText(ds.getNameFromID(inspectorID));
//
//            String title = ds.getTitleFromID(inspectorID);
//            String name = ds.getNameFromID(inspectorID);
//
//            signatures = PopUp.signature.areaOwner;
//            PopUp.showInspectorPopUp(mActivity, inspectorID, name, title, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
//                    registerET.getText().toString());
//            ds.close();
//        } else if (requestCode == REQUEST_SCAN_BARCODE_AO
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }
//
//        if (requestCode == REQUEST_SCAN_BARCODE_MAINRES
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            ugmrET.setText(ds.getNameFromID(inspectorID));
//
//            String title = ds.getTitleFromID(inspectorID);
//            String name = ds.getNameFromID(inspectorID);
//
//            signatures = PopUp.signature.mainRes;
//            PopUp.showInspectorPopUp(mActivity, inspectorID, name, title, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
//                    registerET.getText().toString());
//            ds.close();
//        } else if (requestCode == REQUEST_SCAN_BARCODE_MAINRES
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }
//
//        if (requestCode == REQUEST_SCAN_BARCODE_CSE
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            cseET.setText(ds.getNameFromID(inspectorID));
//
//            String title = ds.getTitleFromID(inspectorID);
//            String name = ds.getNameFromID(inspectorID);
//
//            signatures = PopUp.signature.CSE;
//            PopUp.showInspectorPopUp(mActivity, inspectorID, name, title, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
//                    registerET.getText().toString());
//            ds.close();
//        }else if (requestCode == REQUEST_SCAN_BARCODE_CSE
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }

        if (requestCode == REQUEST_SCAN_BARCODE_DEPT
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            deptHeadET.setText(ds.getNameFromID(inspectorID));

            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.deptHead;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
                        registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_DEPT
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

//        if (requestCode == REQUEST_SCAN_BARCODE_CLIENT
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            clientET.setText(ds.getNameFromID(inspectorID));
//            ds.close();
//        } else if (requestCode == REQUEST_SCAN_BARCODE_CLIENT
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }

        if (requestCode == PhotoPopUp.IMAGE_POP_UP_TAKE_PICTURE_CODE
                && resultCode == getActivity().RESULT_OK) {

            if (Helper.fileUri != null) {
                // new File(tempPhotoDir).mkdirs();
                Helper.setPic(PhotoPopUp.getPicture(), Helper.fileUri.getPath());
                PhotoPopUp.setImagePath(Helper.fileUri.getPath());
            }
        } else if (requestCode == PhotoPopUp.IMAGE_POP_UP_GALLERY_CODE
                && resultCode == getActivity().RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            PhotoPopUp.setImagePath(picturePath);
            Helper.setPic(PhotoPopUp.getPicture(), picturePath);
        }

        if (requestCode == Reports.OPEN_PREVIEW) {
            File fileName = new File(
                    FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH
                            + "/"
                            + new SimpleDateFormat("yyyyMMdd")
                            .format(new Date()) + "/"
                            + "Commissioning Sprinkler.pdf");
            if (fileName.exists())
                fileName.delete();
        }
    }

    public static void saveData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        SprinklerModel model = new SprinklerModel();

        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());
        if (insp1.getValue() == 0) {
            model.setQuestion1("Pass");
        } else if (insp1.getValue() == 1) {
            model.setQuestion1("No");
        } else {
            model.setQuestion1("N/A");
        }
        if (insp2.getValue() == 0) {
            model.setQuestion2("Pass");
        } else if (insp2.getValue() == 1) {
            model.setQuestion2("No");
        } else {
            model.setQuestion2("N/A");
        }
        if (insp3.getValue() == 0) {
            model.setQuestion3("Pass");
        } else if (insp3.getValue() == 1) {
            model.setQuestion3("No");
        } else {
            model.setQuestion3("N/A");
        }
        if (insp4.getValue() == 0) {
            model.setQuestion4("Pass");
        } else if (insp4.getValue() == 1) {
            model.setQuestion4("No");
        } else {
            model.setQuestion4("N/A");
        }
        if (insp5.getValue() == 0) {
            model.setQuestion5("Pass");
        } else if (insp5.getValue() == 1) {
            model.setQuestion5("No");
        } else {
            model.setQuestion5("N/A");
        }
        if (insp6.getValue() == 0) {
            model.setQuestion6("Pass");
        } else if (insp6.getValue() == 1) {
            model.setQuestion6("No");
        } else {
            model.setQuestion6("N/A");
        }
        if (insp7.getValue() == 0) {
            model.setQuestion7("Pass");
        } else if (insp7.getValue() == 1) {
            model.setQuestion7("No");
        } else {
            model.setQuestion7("N/A");
        }
        if (insp8.getValue() == 0) {
            model.setQuestion8("Pass");
        } else if (insp8.getValue() == 1) {
            model.setQuestion8("No");
        } else {
            model.setQuestion8("N/A");
        }
        if (insp9.getValue() == 0) {
            model.setQuestion9("Pass");
        } else if (insp9.getValue() == 1) {
            model.setQuestion9("No");
        } else {
            model.setQuestion9("N/A");
        }
        if (insp10.getValue() == 0) {
            model.setQuestion10("Pass");
        } else if (insp10.getValue() == 1) {
            model.setQuestion10("No");
        } else {
            model.setQuestion10("N/A");
        }
        if (insp11.getValue() == 0) {
            model.setQuestion11("Pass");
        } else if (insp11.getValue() == 1) {
            model.setQuestion11("No");
        } else {
            model.setQuestion11("N/A");
        }
        if (insp12.getValue() == 0) {
            model.setQuestion12("Pass");
        } else if (insp12.getValue() == 1) {
            model.setQuestion12("No");
        } else {
            model.setQuestion12("N/A");
        }
        if (insp13.getValue() == 0) {
            model.setQuestion13("Pass");
        } else if (insp13.getValue() == 1) {
            model.setQuestion13("No");
        } else {
            model.setQuestion13("N/A");
        }
        if (insp14.getValue() == 0) {
            model.setQuestion14("Pass");
        } else if (insp14.getValue() == 1) {
            model.setQuestion14("No");
        } else {
            model.setQuestion14("N/A");
        }
        if (insp15.getValue() == 0) {
            model.setQuestion15("Pass");
        } else if (insp15.getValue() == 1) {
            model.setQuestion15("No");
        } else {
            model.setQuestion15("N/A");
        }
        if (insp16.getValue() == 0) {
            model.setQuestion16("Pass");
        } else if (insp16.getValue() == 1) {
            model.setQuestion16("No");
        } else {
            model.setQuestion16("N/A");
        }
        if (insp17.getValue() == 0) {
            model.setQuestion17("Pass");
        } else if (insp17.getValue() == 1) {
            model.setQuestion17("No");
        } else {
            model.setQuestion17("N/A");
        }
        if (insp18.getValue() == 0) {
            model.setQuestion18("Pass");
        } else if (insp18.getValue() == 1) {
            model.setQuestion18("No");
        } else {
            model.setQuestion18("N/A");
        }
        model.setRemark1(remark1ET.getText().toString());
        model.setRemark2(remark2ET.getText().toString());
        model.setRemark3(remark3ET.getText().toString());
        model.setRemark4(remark4ET.getText().toString());
        model.setRemark5(remark5ET.getText().toString());
        model.setRemark6(remark6ET.getText().toString());
        model.setRemark7(remark7ET.getText().toString());
        model.setRemark8(remark8ET.getText().toString());
        model.setRemark9(remark9ET.getText().toString());
        model.setRemark10(remark10ET.getText().toString());
        model.setRemark11(remark11ET.getText().toString());
        model.setRemark12(remark12ET.getText().toString());
        model.setRemark13(remark13ET.getText().toString());
        model.setRemark14(remark14ET.getText().toString());
        model.setRemark15(remark15ET.getText().toString());
        model.setRemark16(remark16ET.getText().toString());
        model.setRemark17(remark17ET.getText().toString());
        model.setRemark18(remark18ET.getText().toString());

        ds.insertCommissioning(model);
        ds.close();
    }

    public static void updateData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        SprinklerModel model = new SprinklerModel();

        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());
        if (insp1.getValue() == 0) {
            model.setQuestion1("Pass");
        } else if (insp1.getValue() == 1) {
            model.setQuestion1("No");
        } else {
            model.setQuestion1("N/A");
        }
        if (insp2.getValue() == 0) {
            model.setQuestion2("Pass");
        } else if (insp2.getValue() == 1) {
            model.setQuestion2("No");
        } else {
            model.setQuestion2("N/A");
        }
        if (insp3.getValue() == 0) {
            model.setQuestion3("Pass");
        } else if (insp3.getValue() == 1) {
            model.setQuestion3("No");
        } else {
            model.setQuestion3("N/A");
        }
        if (insp4.getValue() == 0) {
            model.setQuestion4("Pass");
        } else if (insp4.getValue() == 1) {
            model.setQuestion4("No");
        } else {
            model.setQuestion4("N/A");
        }
        if (insp5.getValue() == 0) {
            model.setQuestion5("Pass");
        } else if (insp5.getValue() == 1) {
            model.setQuestion5("No");
        } else {
            model.setQuestion5("N/A");
        }
        if (insp6.getValue() == 0) {
            model.setQuestion6("Pass");
        } else if (insp6.getValue() == 1) {
            model.setQuestion6("No");
        } else {
            model.setQuestion6("N/A");
        }
        if (insp7.getValue() == 0) {
            model.setQuestion7("Pass");
        } else if (insp7.getValue() == 1) {
            model.setQuestion7("No");
        } else {
            model.setQuestion7("N/A");
        }
        if (insp8.getValue() == 0) {
            model.setQuestion8("Pass");
        } else if (insp8.getValue() == 1) {
            model.setQuestion8("No");
        } else {
            model.setQuestion8("N/A");
        }
        if (insp9.getValue() == 0) {
            model.setQuestion9("Pass");
        } else if (insp9.getValue() == 1) {
            model.setQuestion9("No");
        } else {
            model.setQuestion9("N/A");
        }
        if (insp10.getValue() == 0) {
            model.setQuestion10("Pass");
        } else if (insp10.getValue() == 1) {
            model.setQuestion10("No");
        } else {
            model.setQuestion10("N/A");
        }
        if (insp11.getValue() == 0) {
            model.setQuestion11("Pass");
        } else if (insp11.getValue() == 1) {
            model.setQuestion11("No");
        } else {
            model.setQuestion11("N/A");
        }
        if (insp12.getValue() == 0) {
            model.setQuestion12("Pass");
        } else if (insp12.getValue() == 1) {
            model.setQuestion12("No");
        } else {
            model.setQuestion12("N/A");
        }
        if (insp13.getValue() == 0) {
            model.setQuestion13("Pass");
        } else if (insp13.getValue() == 1) {
            model.setQuestion13("No");
        } else {
            model.setQuestion13("N/A");
        }
        if (insp14.getValue() == 0) {
            model.setQuestion14("Pass");
        } else if (insp14.getValue() == 1) {
            model.setQuestion14("No");
        } else {
            model.setQuestion14("N/A");
        }
        if (insp15.getValue() == 0) {
            model.setQuestion15("Pass");
        } else if (insp15.getValue() == 1) {
            model.setQuestion15("No");
        } else {
            model.setQuestion15("N/A");
        }
        if (insp16.getValue() == 0) {
            model.setQuestion16("Pass");
        } else if (insp16.getValue() == 1) {
            model.setQuestion16("No");
        } else {
            model.setQuestion16("N/A");
        }
        if (insp17.getValue() == 0) {
            model.setQuestion17("Pass");
        } else if (insp17.getValue() == 1) {
            model.setQuestion17("No");
        } else {
            model.setQuestion17("N/A");
        }
        if (insp18.getValue() == 0) {
            model.setQuestion18("Pass");
        } else if (insp18.getValue() == 1) {
            model.setQuestion18("No");
        } else {
            model.setQuestion18("N/A");
        }
        model.setRemark1(remark1ET.getText().toString());
        model.setRemark2(remark2ET.getText().toString());
        model.setRemark3(remark3ET.getText().toString());
        model.setRemark4(remark4ET.getText().toString());
        model.setRemark5(remark5ET.getText().toString());
        model.setRemark6(remark6ET.getText().toString());
        model.setRemark7(remark7ET.getText().toString());
        model.setRemark8(remark8ET.getText().toString());
        model.setRemark9(remark9ET.getText().toString());
        model.setRemark10(remark10ET.getText().toString());
        model.setRemark11(remark11ET.getText().toString());
        model.setRemark12(remark12ET.getText().toString());
        model.setRemark13(remark13ET.getText().toString());
        model.setRemark14(remark14ET.getText().toString());
        model.setRemark15(remark15ET.getText().toString());
        model.setRemark16(remark16ET.getText().toString());
        model.setRemark17(remark17ET.getText().toString());
        model.setRemark18(remark18ET.getText().toString());

        ds.updateCommissioningData(model);
        ds.close();
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from Commissioning Sprinkler Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("SAVE AND EXIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (validationGenerate()) {
                    saveData();
                    mActivity.finish();
                }
            }
        });

        builder.show();
    }
}
