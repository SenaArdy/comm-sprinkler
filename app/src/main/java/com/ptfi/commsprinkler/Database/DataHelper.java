package com.ptfi.commsprinkler.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ptfi.commsprinkler.Utils.FoldersFilesName;

/**
 * Created by senaardyputra on 6/16/16.
 */
public class DataHelper extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "CommissioningSprinkler.db";
    private static final int DATABASE_VERSION = 5;

    public static final String INSPECTOR_TABLE = "inspector_table";
    public static final String INSPECTOR_NUMBER = "_id";
    public static final String INSPECTOR_NAME = "inspector_name";
    public static final String INSPECTOR_ID = "inspector_id";
    public static final String INSPECTOR_TITLE = "inspector_title";
    public static final String INSPECTOR_ORG = "inspector_org";

    public static final String DATABASE_INSPECTOR = "create table " + INSPECTOR_TABLE + "("
            + INSPECTOR_NUMBER + " integer primary key autoincrement , " + INSPECTOR_NAME + " text , "
            + INSPECTOR_ID + " text , " + INSPECTOR_TITLE + " text , " + INSPECTOR_ORG +" text " + ");";

    public static final String COMMISSIONING_TABLE  = "commissioning_table";
    public static final String COMMISSIONING_ID = "_id";
    public static final String COMMISSIONING_EQUIPMENT  = "equipment";
    public static final String COMMISSIONING_LOCATION  = "location";
    public static final String COMMISSIONING_DATE  = "date";
    public static final String COMMISSIONING_TYPE  = "type";
    public static final String COMMISSIONING_REGISTER = "register";
    public static final String COMMISSIONING_CLIENT  = "client";
    public static final String COMMISSIONING_QUESTION1  = "q1";
    public static final String COMMISSIONING_REMARK1  = "r1";
    public static final String COMMISSIONING_QUESTION2 = "q2";
    public static final String COMMISSIONING_REMARK2  = "r2";
    public static final String COMMISSIONING_QUESTION3  = "q3";
    public static final String COMMISSIONING_REMARK3  = "r3";
    public static final String COMMISSIONING_QUESTION4  = "q4";
    public static final String COMMISSIONING_REMARK4  = "r4";
    public static final String COMMISSIONING_QUESTION5  = "q5";
    public static final String COMMISSIONING_REMARK5  = "r5";
    public static final String COMMISSIONING_QUESTION6  = "q6";
    public static final String COMMISSIONING_REMARK6  = "r6";
    public static final String COMMISSIONING_QUESTION7  = "q7";
    public static final String COMMISSIONING_REMARK7  = "r7";
    public static final String COMMISSIONING_QUESTION8  = "q8";
    public static final String COMMISSIONING_REMARK8  = "r8";
    public static final String COMMISSIONING_QUESTION9  = "q9";
    public static final String COMMISSIONING_REMARK9  = "r9";
    public static final String COMMISSIONING_QUESTION10  = "q10";
    public static final String COMMISSIONING_REMARK10  = "r10";
    public static final String COMMISSIONING_QUESTION11  = "q11";
    public static final String COMMISSIONING_REMARK11  = "r11";
    public static final String COMMISSIONING_QUESTION12  = "q12";
    public static final String COMMISSIONING_REMARK12  = "r12";
    public static final String COMMISSIONING_QUESTION13  = "q13";
    public static final String COMMISSIONING_REMARK13  = "r13";
    public static final String COMMISSIONING_QUESTION14  = "q14";
    public static final String COMMISSIONING_REMARK14  = "r14";
    public static final String COMMISSIONING_QUESTION15  = "q15";
    public static final String COMMISSIONING_REMARK15  = "r15";
    public static final String COMMISSIONING_QUESTION16  = "q16";
    public static final String COMMISSIONING_REMARK16  = "r16";
    public static final String COMMISSIONING_QUESTION17  = "q17";
    public static final String COMMISSIONING_REMARK17  = "r17";
    public static final String COMMISSIONING_QUESTION18  = "q18";
    public static final String COMMISSIONING_REMARK18  = "r18";
    public static final String COMMISSIONING_NAME_ENG  = "name_engineering";
    public static final String COMMISSIONING_ID_ENG  = "id_engineering";
    public static final String COMMISSIONING_SIGN_ENG  = "sign_engineering";
    public static final String COMMISSIONING_NAME_MAIN  = "name_maintenance";
    public static final String COMMISSIONING_ID_MAIN  = "id_maintenance";
    public static final String COMMISSIONING_SIGN_MAIN  = "sign_maintenance";
    public static final String COMMISSIONING_NAME_AO  = "name_areaowner";
    public static final String COMMISSIONING_ID_AO  = "id_areaowner";
    public static final String COMMISSIONING_SIGN_AO  = "sign_areaowner";
    public static final String COMMISSIONING_NAME_MAINRES  = "name_mainrescue";
    public static final String COMMISSIONING_ID_MAINRES  = "id_mainrescue";
    public static final String COMMISSIONING_SIGN_MAINRES  = "sign_mainrescue";
    public static final String COMMISSIONING_NAME_CSE  = "name_cse";
    public static final String COMMISSIONING_ID_CSE = "id_cse";
    public static final String COMMISSIONING_SIGN_CSE  = "sign_cse";
    public static final String COMMISSIONING_NAME_DEPT  = "name_dept";
    public static final String COMMISSIONING_ID_DEPT  = "id_dept";
    public static final String COMMISSIONING_SIGN_DEPT  = "sign_dept";
    public static final String COMMISSIONING_DIVISON = "division";
    public static final String COMMISSIONING_CONTRACTOR = "contractor";

    public static final String DATABASE_COMMISSIONING = "create table " +COMMISSIONING_TABLE + "("
            + COMMISSIONING_ID + " integer primary key autoincrement , " + COMMISSIONING_EQUIPMENT + " text , "
            + COMMISSIONING_LOCATION + " text , " + COMMISSIONING_DATE + " text , " + COMMISSIONING_TYPE + " text , "
            + COMMISSIONING_REGISTER + " text , " + COMMISSIONING_CLIENT + " text , " + COMMISSIONING_QUESTION1 + " text , "
            + COMMISSIONING_REMARK1 + " text , " + COMMISSIONING_QUESTION2 + " text , " + COMMISSIONING_REMARK2 + " text , "
            + COMMISSIONING_QUESTION3 + " text , " + COMMISSIONING_REMARK3 + " text , " + COMMISSIONING_QUESTION4 + " text , "
            + COMMISSIONING_REMARK4 + " text , " + COMMISSIONING_QUESTION5 + " text , " + COMMISSIONING_REMARK5 + " text , "
            + COMMISSIONING_QUESTION6 + " text , " + COMMISSIONING_REMARK6 + " text , " + COMMISSIONING_QUESTION7 + " text , "
            + COMMISSIONING_REMARK7 + " text , " + COMMISSIONING_QUESTION8 + " text , " + COMMISSIONING_REMARK8 + " text , "
            + COMMISSIONING_QUESTION9 + " text , " + COMMISSIONING_REMARK9 + " text , " + COMMISSIONING_QUESTION10 + " text , "
            + COMMISSIONING_REMARK10 + " text , "+ COMMISSIONING_QUESTION11 + " text , " + COMMISSIONING_REMARK11 + " text , "
            + COMMISSIONING_QUESTION12 + " text , " + COMMISSIONING_REMARK12 + " text , " + COMMISSIONING_QUESTION13 + " text , "
            + COMMISSIONING_REMARK13 + " text , " + COMMISSIONING_QUESTION14 + " text , " + COMMISSIONING_REMARK14 + " text , "
            + COMMISSIONING_QUESTION15 + " text , " + COMMISSIONING_REMARK15 + " text , " + COMMISSIONING_QUESTION16 + " text , "
            + COMMISSIONING_REMARK16 + " text , " + COMMISSIONING_QUESTION17 + " text , " + COMMISSIONING_REMARK17 + " text , "
            + COMMISSIONING_QUESTION18 + " text , " + COMMISSIONING_REMARK18 + " text , " + COMMISSIONING_NAME_ENG + " text , "
            + COMMISSIONING_ID_ENG + " text , " + COMMISSIONING_SIGN_ENG + " text , " + COMMISSIONING_NAME_MAIN + " text , "
            + COMMISSIONING_ID_MAIN + " text , " + COMMISSIONING_SIGN_MAIN + " text , " + COMMISSIONING_NAME_AO + " text , "
            + COMMISSIONING_ID_AO + " text , " + COMMISSIONING_SIGN_AO + " text , " + COMMISSIONING_NAME_MAINRES + " text , "
            + COMMISSIONING_ID_MAINRES + " text , " + COMMISSIONING_SIGN_MAINRES + " text , " + COMMISSIONING_NAME_CSE + " text , "
            + COMMISSIONING_ID_CSE + " text , " + COMMISSIONING_SIGN_CSE + " text , " + COMMISSIONING_NAME_DEPT + " text , "
            + COMMISSIONING_ID_DEPT + " text , " + COMMISSIONING_SIGN_DEPT + " text , " + COMMISSIONING_DIVISON + " text , "
            + COMMISSIONING_CONTRACTOR + " text " + ");";

    public static final String PHOTO_TABLE = "photo_table";
    public static final String PHOTO_ID = "_id";
    public static final String PHOTO_PATH = "photo_path";
    public static final String PHOTO_TITLE = "photo_title";
    public static final String PHOTO_COMMENT = "photo_comment";
    public static final String PHOTO_EQUIPMENT= "photo_equipment";
    public static final String PHOTO_DATE = "photo_date";
    public static final String PHOTO_REGISTER = "photo_register";

    public static final String DATABASE_PHOTO = "create table " + PHOTO_TABLE + "("
            +PHOTO_ID + " integer primary key autoincrement , " + PHOTO_PATH + " text , " + PHOTO_TITLE + " text , "
            + PHOTO_COMMENT + " text , " + PHOTO_EQUIPMENT + " text , " + PHOTO_DATE + " text , " + PHOTO_REGISTER + " text " + ");";

    public static final String COMPLIANCE_TABLE = "compliance_table";
    public static final String COMPLIANCE_ID = "_id";
    public static final String COMPLIANCE_FINDINGS = "findings";
    public static final String COMPLIANCE_REMARK = "remark";
    public static final String COMPLIANCE_RESPONSIBILITY = "responsibility";
    public static final String COMPLIANCE_DONE = "done";
    public static final String COMPLIANCE_EQUIPMENT= "compliance_equipment";
    public static final String COMPLIANCE_DATE = "compliance_date";
    public static final String COMPLIANCE_REGISTER = "compliance_register";

    public static final String DATABASE_COMPLIANCE = "create table " + COMPLIANCE_TABLE + "("
            + COMPLIANCE_ID + " integer primary key autoincrement , " + COMPLIANCE_FINDINGS + " text , "
            + COMPLIANCE_REMARK + " text , " + COMPLIANCE_RESPONSIBILITY + " text , "
            + COMPLIANCE_DONE + " text , " + COMPLIANCE_EQUIPMENT + " text , " + COMPLIANCE_DATE + " text , "
            + COMPLIANCE_REGISTER + " text " + ");";

    public static final String HANDOVER_TABLE = "handover_table";
    public static final String HANDOVER_ID = "_id";
    public static final String HANDOVER_EQUIPMENT = "equipment";
    public static final String HANDOVER_LOCATION  = "location";
    public static final String HANDOVER_DATE  = "date";
    public static final String HANDOVER_TYPE  = "type";
    public static final String HANDOVER_REGISTER = "register";
    public static final String HANDOVER_CLIENT  = "client";
    public static final String HANDOVER_NAME_ENG  = "name_engineering";
    public static final String HANDOVER_ID_ENG  = "id_engineering";
    public static final String HANDOVER_SIGN_ENG  = "sign_engineering";
    public static final String HANDOVER_NAME_MAIN  = "name_maintenance";
    public static final String HANDOVER_ID_MAIN  = "id_maintenance";
    public static final String HANDOVER_SIGN_MAIN  = "sign_maintenance";
    public static final String HANDOVER_NAME_AO  = "name_areaowner";
    public static final String HANDOVER_ID_AO  = "id_areaowner";
    public static final String HANDOVER_SIGN_AO  = "sign_areaowner";
    public static final String HANDOVER_NAME_MAINRES  = "name_mainrescue";
    public static final String HANDOVER_ID_MAINRES  = "id_mainrescue";
    public static final String HANDOVER_SIGN_MAINRES  = "sign_mainrescue";
    public static final String HANDOVER_NAME_CSE  = "name_cse";
    public static final String HANDOVER_ID_CSE = "id_cse";
    public static final String HANDOVER_SIGN_CSE  = "sign_cse";
    public static final String HANDOVER_NAME_DEPT  = "name_dept";
    public static final String HANDOVER_ID_DEPT  = "id_dept";
    public static final String HANDOVER_SIGN_DEPT  = "sign_dept";
    public static final String HANDOVER_DIVISON = "division";
    public static final String HANDOVER_CONTRACTOR = "contractor";

    public static final String DATABASE_HANDOVER = "create table " + HANDOVER_TABLE + "("
            + HANDOVER_ID + " integer primary key autoincrement , " + HANDOVER_EQUIPMENT + " text , "
            + HANDOVER_LOCATION + " text , " + HANDOVER_DATE + " text , " + HANDOVER_TYPE + " text , "
            + HANDOVER_REGISTER + " text , " + HANDOVER_CLIENT + " text , " + HANDOVER_NAME_ENG + " text , "
            + HANDOVER_ID_ENG + " text , " + HANDOVER_SIGN_ENG + " text , " + HANDOVER_NAME_MAIN + " text , "
            + HANDOVER_ID_MAIN + " text , " + HANDOVER_SIGN_MAIN + " text , " + HANDOVER_NAME_AO + " text , "
            + HANDOVER_ID_AO + " text , " + HANDOVER_SIGN_AO + " text , " + HANDOVER_NAME_MAINRES + " text , "
            + HANDOVER_ID_MAINRES + " text , " + HANDOVER_SIGN_MAINRES + " text , " + HANDOVER_NAME_CSE + " text , "
            + HANDOVER_ID_CSE + " text , " +    HANDOVER_SIGN_CSE + " text , " + HANDOVER_NAME_DEPT + " text , "
            + HANDOVER_ID_DEPT + " text , " + HANDOVER_SIGN_DEPT + " text , " + HANDOVER_DIVISON + " text , "
            + HANDOVER_CONTRACTOR + " text " + ");";

    public static final String TERMS_TABLE = "terms_table";
    public static final String TERMS_ID = "_id";
    public static final String TERMS_TERMS = "terms";
    public static final String TERMS_EQUIPMENT= "terms_equipment";
    public static final String TERMS_DATE = "terms_date";
    public static final String TERMS_REGISTER = "terms_register";

    public static final String DATABASE_TERMS = "create table " + TERMS_TABLE + "("
            + TERMS_ID + " integer primary key autoincrement , " + TERMS_TERMS + " text , "
            + TERMS_EQUIPMENT + " text , " + TERMS_DATE + " text , " + TERMS_REGISTER + " text " + ");";

    public DataHelper(Context context) {
        super(context, FoldersFilesName.DB_FOLDER_ON_EXTERNAL_PATH + "/"
                + DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_INSPECTOR);
        db.execSQL(DATABASE_COMMISSIONING);
        db.execSQL(DATABASE_PHOTO);
        db.execSQL(DATABASE_COMPLIANCE);
        db.execSQL(DATABASE_HANDOVER);
        db.execSQL(DATABASE_TERMS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + INSPECTOR_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + COMMISSIONING_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + PHOTO_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + COMPLIANCE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + HANDOVER_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + TERMS_TABLE);
        onCreate(db);
    }
}
