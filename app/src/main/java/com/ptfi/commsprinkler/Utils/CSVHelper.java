package com.ptfi.commsprinkler.Utils;

import android.app.Activity;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by senaardyputra on 6/17/16.
 */
public class CSVHelper {

    public static void appendCSV(String path,String filename, List<String[]> values)
    {
        List<String[]> previousList = CSVHelper.readCSVFromPath(path);
        previousList.addAll(values);
        CSVHelper.writeCSV(path,filename, previousList);
    }

    public static boolean writeCSV(String path,String filename,List<String[]> values)
    {
        boolean status = true;
        File f = new File(path);
        if(!(f.exists() && f.isDirectory()))
        {
            f.mkdirs();
        }

        CSVWriter writer;
        try {
            writer = new CSVWriter(new FileWriter(path+filename));
            writer.writeAll(values);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }
    public static List<String[]> readCSVFromPath(String path)
    {
        List<String[]> returnValue=null;

        try {
            CSVReader reader = new CSVReader(new FileReader(path));
            if(reader != null)
            {
                returnValue = reader.readAll();
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return returnValue;

    }

    public static class Csv {
        public static String Escape(String s) {
            if (s.contains(QUOTE))
                s = s.replace(QUOTE, ESCAPED_QUOTE);

            if (indexOfAny(s, CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                s = QUOTE + s + QUOTE;

            if (s.contains(ENTER))
                s = s.replace(ENTER, SPACE);

            return s;
        }

        public static String Unescape(String s) {
            if (s.startsWith(QUOTE) && s.endsWith(QUOTE)) {
                s = s.substring(1, s.length() - 2);

                if (s.contains(ESCAPED_QUOTE))
                    s = s.replace(ESCAPED_QUOTE, QUOTE);
            }

            return s;
        }

        private static String QUOTE = "\"";
        private static String ESCAPED_QUOTE = "\"\"";
        private static String ENTER = "\n";
        private static String SPACE = " ";
        private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };

        public static int indexOfAny(String str, char[] searchChars) {
            if (isEmpty(str) || isEmpty(searchChars)) {
                return -1;
            }
            for (int i = 0; i < str.length(); i++) {
                char ch = str.charAt(i);
                for (int j = 0; j < searchChars.length; j++) {
                    if (searchChars[j] == ch) {
                        return i;
                    }
                }
            }
            return -1;
        }

        public static boolean isEmpty(char[] array) {
            if (array == null || array.length == 0) {
                return true;
            }
            return false;
        }

        public static boolean isEmpty(String str) {
            return str == null || str.length() == 0;
        }
    }

//    public static void exportCSV(Activity mActivity, ArrayList<String[]> lookUpValue, Helper.Lookup lookupType) {
//        ArrayList<String[]> transformedData = new ArrayList<>();
//        String exportName = "";
//        String[] header = null;
//
//        switch (lookupType) {
//            case equipmentData:
//                header = new String[] {"equipment_register", "serial_number", "equipment_type", "equipment_name", "equipment_location",
//                        "inspection_date", "next_inspection"};
//                transformedData.add(header);
//
//                for (String[] strings : lookUpValue) {
//                    String[] data = {Csv.Escape(strings[1]), Csv.Escape(strings[2]), Csv.Escape(strings[5]),
//                            Csv.Escape(strings[4]), Csv.Escape(strings[3]), Csv.Escape(strings[6]), Csv.Escape(strings[7])};
//                    transformedData.add(data);
//                }
//                exportName = "_EQUIPMENTDATA";
//                break;
//
//            case inspectorData:
//                header = new String[] {"CALL_SIGN", "ID_NO", "NAME"};
//                transformedData.add(header);
//
//                for (String[] strings : lookUpValue) {
//                    String[] data = {Csv.Escape(strings[1]), Csv.Escape(strings[2]), Csv.Escape(strings[3])};
//                    transformedData.add(data);
//                }
//
//                exportName = "_INSPECTOR";
//                break;
//
//            case inspectionData:
//                header = new String[] {"equipment_register", "serial_number", "equipment_location", "equipment_name", "equipment_type",
//                        "inspection_date", "next_inspection", "inspector_name", "inspector_id", "inspector_sign", "inspection1", "remark1", "inspection2", "remark2", "inspection3", "remark3",
//                        "inspection4", "remark4", "inspection5", "remark5", "inspection6_1", "remark6_1", "inspection6_2", "remark6_2", "inspection6_3", "remark6_3",
//                        "inspection7", "remark7", "inspection8", "remark8", "inspection9", "remark9", "inspection10", "remark10", "inspection11", "remark11",
//                        "inspection12_1", "remark12_1", "inspection12_2", "remark12_2", "inspection12_3", "remark12_3", "inspection12_4", "remark12_4", "inspection12_5", "remark12_5",
//                        "inspection12_6", "remark12_6", "inspection12_7", "remark12_7", "inspection12_8", "remark12_8", "inspection12_9", "remark12_9", "inspection13_1", "remark13_1",
//                        "inspection13_2", "remark13_2", "inspection13_3", "remark13_3", "inspection13_4", "remark13_4", "inspection13_5", "remark13_5", "inspection14_1", "remark14_1",
//                        "inspection14_2", "remark14_2", "inspection14_3", "remark14_3", "inspection15_1", "remark15_1", "inspection15_2", "remark15_2", "inspection15_3", "remark15_3",
//                        "inspection15_4", "remark15_4", "inspection15_5", "remark15_5", "inspection15_6", "remark15_6", "inspection16_1", "remark16_1", "inspection16_2", "remark16_2",
//                        "inspection16_3", "remark16_3", "inspection16_4", "remark16_4", "inspection16_5", "remark16_5", "inspection16_6", "remark16_6", "inspection17", "remark17",
//                        "note_type", "note_discovery"};
//                transformedData.add(header);
//
//                for (String[] strings : lookUpValue) {
//                    String[] data = {Csv.Escape(strings[1]), Csv.Escape(strings[2]), Csv.Escape(strings[3]), Csv.Escape(strings[4]),
//                            Csv.Escape(strings[5]), Csv.Escape(strings[6]), Csv.Escape(strings[7]), Csv.Escape(strings[8]), Csv.Escape(strings[9]), Csv.Escape(strings[10]),
//                            Csv.Escape(strings[11]), Csv.Escape(strings[12]), Csv.Escape(strings[13]), Csv.Escape(strings[14]), Csv.Escape(strings[15]),
//                            Csv.Escape(strings[16]), Csv.Escape(strings[17]), Csv.Escape(strings[18]), Csv.Escape(strings[19]), Csv.Escape(strings[20]),
//                            Csv.Escape(strings[21]), Csv.Escape(strings[22]), Csv.Escape(strings[23]), Csv.Escape(strings[24]), Csv.Escape(strings[25]),
//                            Csv.Escape(strings[26]), Csv.Escape(strings[27]), Csv.Escape(strings[28]), Csv.Escape(strings[29]), Csv.Escape(strings[30]),
//                            Csv.Escape(strings[31]), Csv.Escape(strings[32]), Csv.Escape(strings[33]), Csv.Escape(strings[34]), Csv.Escape(strings[35]),
//                            Csv.Escape(strings[36]), Csv.Escape(strings[37]), Csv.Escape(strings[38]), Csv.Escape(strings[39]), Csv.Escape(strings[40]),
//                            Csv.Escape(strings[41]), Csv.Escape(strings[42]), Csv.Escape(strings[43]), Csv.Escape(strings[44]), Csv.Escape(strings[45]),
//                            Csv.Escape(strings[46]), Csv.Escape(strings[47]), Csv.Escape(strings[48]), Csv.Escape(strings[49]), Csv.Escape(strings[50]),
//                            Csv.Escape(strings[51]), Csv.Escape(strings[52]), Csv.Escape(strings[53]), Csv.Escape(strings[54]), Csv.Escape(strings[55]),
//                            Csv.Escape(strings[56]), Csv.Escape(strings[57]), Csv.Escape(strings[58]), Csv.Escape(strings[59]), Csv.Escape(strings[60]),
//                            Csv.Escape(strings[61]), Csv.Escape(strings[62]), Csv.Escape(strings[63]), Csv.Escape(strings[64]), Csv.Escape(strings[65]),
//                            Csv.Escape(strings[66]), Csv.Escape(strings[67]), Csv.Escape(strings[68]), Csv.Escape(strings[69]), Csv.Escape(strings[70]),
//                            Csv.Escape(strings[71]), Csv.Escape(strings[72]), Csv.Escape(strings[73]), Csv.Escape(strings[74]), Csv.Escape(strings[75]),
//                            Csv.Escape(strings[76]), Csv.Escape(strings[77]), Csv.Escape(strings[78]), Csv.Escape(strings[79]), Csv.Escape(strings[80]),
//                            Csv.Escape(strings[81]), Csv.Escape(strings[82]), Csv.Escape(strings[83]), Csv.Escape(strings[84]), Csv.Escape(strings[85]),
//                            Csv.Escape(strings[86]), Csv.Escape(strings[87]), Csv.Escape(strings[88]), Csv.Escape(strings[89]), Csv.Escape(strings[90]),
//                            Csv.Escape(strings[91]), Csv.Escape(strings[92]), Csv.Escape(strings[93]), Csv.Escape(strings[94]), Csv.Escape(strings[95]),
//                            Csv.Escape(strings[96]), Csv.Escape(strings[97]), Csv.Escape(strings[98])};
//                    transformedData.add(data);
//                }
//
//                exportName= "_INSPECTION";
//                break;
//
//            default:
//                break;
//        }
//
//        String exportPath = FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH
//                + "/" + FoldersFilesName.LOOKUP_FOLDER_NAME + "/";
//        int i = 0;
//        boolean isContinueAc = true;
//        String filename = "";
//        while (isContinueAc) {
//            filename = new SimpleDateFormat("yyyyMMdd").format(new Date())
//                    + (i == 0 ? (exportName + ".csv")
//                    : (exportName + "_") + String.valueOf(i)
//                    + ".csv");
//            File checkFileAcquire = new File(exportPath + filename);
//
//            if (checkFileAcquire.exists() && isContinueAc) {
//                i++;
//            } else
//                isContinueAc = false;
//        }
//
//        boolean success = true;
//        if (!CSVHelper.writeCSV(exportPath, filename, transformedData)) {
//            success = false;
//        }
//
//        if (success) {
//            Helper.showPopUpMessage(mActivity, "Notification",
//                    "Export Csv Success.\nFile saved in folder " + exportPath,
//                    null);
//        } else {
//            Helper.showPopUpMessage(mActivity, "Notification",
//                    "Export Csv Failed", null);
//        }
//    }
}
