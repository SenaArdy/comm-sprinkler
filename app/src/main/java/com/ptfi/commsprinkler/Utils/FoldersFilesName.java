package com.ptfi.commsprinkler.Utils;

import android.os.Environment;

/**
 * Created by senaardyputra on 6/16/16.
 */
public class FoldersFilesName {

    public static String ROOT_FOLDER_NAME = "com.PTFI";
    public static String APP_FOLDER_NAME = "Commissioning Sprinkler";
    public static String DB_FOLDER_NAME = "Database";
    public static String IMPORT_FOLDER_NAME = "Import";
    public static String EXPORT_FOLDER_NAME = "Export";
    public static String TEMPLATES_FOLDER_NAME = "Templates";
    public static String LOOKUP_FOLDER_NAME = "Look Up";

    public static String INSPECTOR_FILE_NAME = "INSPECTOR.csv";
    public static String DATA_EQUIPMENT_FILE_NAME = "EQUIPMENTDATA.csv";
    public static String INSPECTION_FILE_NAME = "INSPECTION.csv";

    public static String TEMPLATE_COMMISSIONING_SPRINKLER = "template_commissioning_sprinkler.docx";
    public static String TEMPLATE_HANDOVER = "template_handover.docx";

    public static final String VERSION_FILE = "version.info";

    // Root Folder Names
    public static final String EXTERNAL_ROOT_FOLDER = Environment
            .getExternalStorageDirectory().getPath()
            + "/com.PTFI/"
            + APP_FOLDER_NAME + "/";

    public static final String BASEPATH = Environment
            .getExternalStorageDirectory().getPath()
            + "/com.PTFI/";

    // Database Folder and Files
    public static final String DB_FOLDER_ON_EXTERNAL_PATH = EXTERNAL_ROOT_FOLDER
            + DB_FOLDER_NAME;

    // Import Folder and Files
    public static final String IMPORT_FOLDER_ON_EXTERNAL_PATH = EXTERNAL_ROOT_FOLDER
            + IMPORT_FOLDER_NAME;

    // Template Folder and Files
    public static final String TEMPLATES_FOLDER_ON_EXTERNAL_PATH = EXTERNAL_ROOT_FOLDER
            + TEMPLATES_FOLDER_NAME;

    public static final String TEMPLATES_FOLDER_ON_ASSETS_PATH = ROOT_FOLDER_NAME
            + "/"
            + APP_FOLDER_NAME
            + "/"
            + TEMPLATES_FOLDER_NAME;

    // Export Folder and Files
    public static final String EXPORT_FOLDER_ON_EXTERNAL_PATH = EXTERNAL_ROOT_FOLDER
            + EXPORT_FOLDER_NAME;

    public static String tempPhotosDir = BASEPATH + "/" + APP_FOLDER_NAME + "/"
            + EXPORT_FOLDER_NAME + "/" + "temp_photos/";
}
