package com.ptfi.commsprinkler.Utils;

/**
 * Created by senaardyputra on 7/19/16.
 */
public class LanguageConstants {

    public String TOGGLE_BTN = "Indonesia";
    public String Q1 = "Instalasi selesai";
    public String Q2 = "Memakai bahan standar FSME";
    public String Q3 = "Test fungsional";
    public String Q4 = "Ada penggambaran";
    public String Q5 = "Kondisi sistem sprinkler";
    public String Q6 = "Sambungan pipa - pipa";
    public String Q7 = "Katup Sprinkler";
    public String Q8 = "Katup pensuplai";
    public String Q9 = "Pengganti Tekanan";
    public String Q10 = "Pengganti Alur";
    public String Q11 = "Pengganti tamper";
    public String Q12 = "Bell /  Klakson";
    public String Q13 = "Aktuator manual";
    public String Q14 = "Pengukur Tekanan";
    public String Q15 = "Kepala Sprinkler";
    public String Q16 = "Periksa Katup";
    public String Q17 = "Katup pemeriksa";
    public String Q18 = "Penanda dan karet penutup";

    public LanguageConstants(boolean isEnglish) {
        if (isEnglish) {
             TOGGLE_BTN = "English";
             Q1 = "Installation complete";
             Q2 = "Material use meet FSME standard";
             Q3 = "Function test";
             Q4 = "Drawing available";
             Q5 = "Sprinkler system condition";
             Q6 = "Pipe connections";
             Q7 = "Sprinkler valve";
             Q8 = "Supply valve";
             Q9 = "Pressure switches";
             Q10 = "Flow switches";
             Q11 = "Temper switches";
             Q12 = "Bells /  Horns";
             Q13 = "Manual actuators";
             Q14 = "Pressure gauge";
             Q15 = "Head sprinkler";
             Q16 = "Check valves";
             Q17 = "Inspector valves";
             Q18 = "Tags and seals";
        } else {
            TOGGLE_BTN = "Indonesia";
            Q1 = "Instalasi selesai";
            Q2 = "Memakai bahan standar FSME";
            Q3 = "Test fungsional";
            Q4 = "Ada penggambaran";
            Q5 = "Kondisi sistem sprinkler";
            Q6 = "Sambungan pipa - pipa";
            Q7 = "Katup Sprinkler";
            Q8 = "Katup pensuplai";
            Q9 = "Pengganti Tekanan";
            Q10 = "Pengganti Alur";
            Q11 = "Pengganti tamper";
            Q12 = "Bell /  Klakson";
            Q13 = "Aktuator manual";
            Q14 = "Pengukur Tekanan";
            Q15 = "Kepala Sprinkler";
            Q16 = "Periksa Katup";
            Q17 = "Katup pemeriksa";
            Q18 = "Penanda dan karet penutup";
        }
    }
}
