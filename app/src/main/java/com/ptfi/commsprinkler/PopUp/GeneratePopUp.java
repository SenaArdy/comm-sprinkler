package com.ptfi.commsprinkler.PopUp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.client.android.CaptureActivity;
import com.ptfi.commsprinkler.Adapter.ListComplianceAdapter;
import com.ptfi.commsprinkler.Adapter.ListPhotoAdapter;
import com.ptfi.commsprinkler.Adapter.ListTermsAdapter;
import com.ptfi.commsprinkler.Database.DataSource;
import com.ptfi.commsprinkler.Models.ComplianceModel;
import com.ptfi.commsprinkler.Models.DataSingleton;
import com.ptfi.commsprinkler.Models.HandoverModel;
import com.ptfi.commsprinkler.Models.InspectorModel;
import com.ptfi.commsprinkler.Models.PhotoModel;
import com.ptfi.commsprinkler.Models.SprinklerModel;
import com.ptfi.commsprinkler.Models.TermsModel;
import com.ptfi.commsprinkler.R;
import com.ptfi.commsprinkler.Utils.Helper;
import com.ptfi.commsprinkler.Utils.LanguageConstants;
import com.ptfi.commsprinkler.Utils.Reports;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by senaardyputra on 7/25/16.
 */
public class GeneratePopUp {

    static String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    static int REQUEST_SCAN_BARCODE_CLIENT = 347;
    static int REQUEST_SCAN_BARCODE_ENG = 348;
    static int REQUEST_SCAN_BARCODE_MAIN = 349;
    static int REQUEST_SCAN_BARCODE_AO = 350;
    static int REQUEST_SCAN_BARCODE_MAINRES = 351;
    static int REQUEST_SCAN_BARCODE_CSE = 352;
    static int REQUEST_SCAN_BARCODE_DEPT = 353;

    static int REQUEST_SCAN_BARCODE_CLIENT_HO = 354;
    static int REQUEST_SCAN_BARCODE_ENG_HO = 355;
    static int REQUEST_SCAN_BARCODE_MAIN_HO = 356;
    static int REQUEST_SCAN_BARCODE_AO_HO = 357;
    static int REQUEST_SCAN_BARCODE_MAINRES_HO = 358;
    static int REQUEST_SCAN_BARCODE_CSE_HO = 359;
    static int REQUEST_SCAN_BARCODE_DEPT_HO = 360;

    static PhotoModel activeFModel = null;

    Dialog activeDialog;

    static String inspectorIDFalse;

    static ArrayList<InspectorModel> dataInspector;

    public static EditText equipmentET, dateET, clientET, typeET, registerET, locationET, remark1ET, remark2ET, remark3ET, remark4ET, remark5ET, remark6ET, remark7ET, remark8ET,
            remark9ET, remark10ET, remark11ET, remark12ET, remark13ET, remark14ET, remark15ET, remark16ET, remark17ET, remark18ET, engineeringET, maintenanceET,
            areaOwnerET, ugmrET, cseET, deptHeadET;

    static TextInputLayout equipmentTL, dateTL, clientTL, typeTL, registerTL, locationTL, remark1TL, remark2TL, remark3TL, remark4TL, remark5TL, remark6TL, remark7TL, remark8TL,
            remark9TL, remark10TL, remark11TL, remark12TL, remark13TL, remark14TL, remark15TL, remark16TL, remark17TL, remark18TL, engineeringTL, maintenanceTL,
            areaOwnerTL, ugmrTL, cseTL, deptHeadTL;

    static MultiStateToggleButton insp1, insp2, insp3, insp4, insp5, insp6, insp7, insp8, insp9, insp10, insp11, insp12, insp13,
            insp14, insp15, insp16, insp17, insp18;

    static TextView Q1TV, Q2TV, Q3TV, Q4TV, Q5TV, Q6TV, Q7TV, Q8TV, Q9TV, Q10TV, Q11TV, Q12TV, Q13TV, Q14TV, Q15TV, Q16TV,
            Q17TV, Q18TV, Q60TV, Q70TV, Q80TV, Q90TV, Q100TV;

    static ImageView addComplianceIV, addPhotosIV;

    static LinearLayout findingsLL, photosLL;

    static RecyclerView findingsRV, photosRV;

    static TextView imeiCode, versionCode;

    static CardView generateCV, previewCV;

    static PopUp.page pages;
    static PopUp.signature signatures;

    static RecyclerView.LayoutManager layoutManager;
    static RecyclerView.LayoutManager photosLayoutManager;

    static ArrayList<ComplianceModel> complianceData = new ArrayList<>();
    static ArrayList<PhotoModel> photoData = new ArrayList<>();

    static ListComplianceAdapter adapterData;
    static ListPhotoAdapter adapterPhotosData;

    static int selectedYear = 0;
    static int selectedMonth = 0;
    static int selectedDate = 0;

    static LanguageConstants languageConstants;
    static android.widget.ToggleButton languageToggleButton;
    static MultiStateToggleButton[] compilanceBtns;
    static int[] compilanceLastValue;
    static boolean isChangedLanguage;

    static EditText addTermsET;

    static TextInputLayout addTermsTL;

    static LinearLayout termsLL;

    static RecyclerView termsRV;

    static CardView addCV;

    static ListTermsAdapter adapterTermData;

    private static ArrayList<TermsModel> termsData = new ArrayList<>();

    public static void commissioningPopUp(final Activity mActivity, final String equipment, final String date, final String location,
                                          final String type, final String register) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_main);

                versionCode = (TextView) dialog.findViewById(R.id.versionCode);
                PackageInfo pInfo;
                try {
                    pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
                    String version = pInfo.versionName;
                    versionCode.setText("Version : " + version);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                // Imei
                TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
                String deviceID = telephonyManager.getDeviceId();
                imeiCode = (TextView) dialog.findViewById(R.id.imeiCode);
                imeiCode.setText("Device IMEI : " + deviceID);

                equipmentET = (EditText) dialog.findViewById(R.id.equipmentET);
                registerET = (EditText) dialog.findViewById(R.id.registerET);
                dateET = (EditText) dialog.findViewById(R.id.dateET);
                clientET = (EditText) dialog.findViewById(R.id.clientET);
                typeET = (EditText) dialog.findViewById(R.id.typeEqET);
                locationET = (EditText) dialog.findViewById(R.id.locationEqET);

                equipmentTL = (TextInputLayout) dialog.findViewById(R.id.equipmentTL);
                registerTL = (TextInputLayout) dialog.findViewById(R.id.registerTL);
                dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);
                clientTL = (TextInputLayout) dialog.findViewById(R.id.clientTL);
                typeTL = (TextInputLayout) dialog.findViewById(R.id.typeEqTL);
                locationTL = (TextInputLayout) dialog.findViewById(R.id.locationEqTL);

                engineeringET = (EditText) dialog.findViewById(R.id.engineeringET);
                maintenanceET = (EditText) dialog.findViewById(R.id.maintenanceET);
                areaOwnerET = (EditText) dialog.findViewById(R.id.areaOwnerET);
                ugmrET = (EditText) dialog.findViewById(R.id.ugmrET);
                cseET = (EditText) dialog.findViewById(R.id.cseET);
                deptHeadET = (EditText) dialog.findViewById(R.id.deptHeadET);

                engineeringTL = (TextInputLayout) dialog.findViewById(R.id.engineeringTL);
                maintenanceTL = (TextInputLayout) dialog.findViewById(R.id.maintenanceTL);
                areaOwnerTL = (TextInputLayout) dialog.findViewById(R.id.areaOwnerTL);
                ugmrTL = (TextInputLayout) dialog.findViewById(R.id.ugmrTL);
                cseTL = (TextInputLayout) dialog.findViewById(R.id.cseTL);
                deptHeadTL = (TextInputLayout) dialog.findViewById(R.id.deptHeadTL);

                remark1ET = (EditText) dialog.findViewById(R.id.remark1ET);
                remark2ET = (EditText) dialog.findViewById(R.id.remark2ET);
                remark3ET = (EditText) dialog.findViewById(R.id.remark3ET);
                remark4ET = (EditText) dialog.findViewById(R.id.remark4ET);
                remark5ET = (EditText) dialog.findViewById(R.id.remark5ET);
                remark6ET = (EditText) dialog.findViewById(R.id.remark6ET);
                remark7ET = (EditText) dialog.findViewById(R.id.remark7ET);
                remark8ET = (EditText) dialog.findViewById(R.id.remark8ET);
                remark9ET = (EditText) dialog.findViewById(R.id.remark9ET);
                remark10ET = (EditText) dialog.findViewById(R.id.remark10ET);
                remark11ET = (EditText) dialog.findViewById(R.id.remark11ET);
                remark12ET = (EditText) dialog.findViewById(R.id.remark12ET);
                remark13ET = (EditText) dialog.findViewById(R.id.remark13ET);
                remark14ET = (EditText) dialog.findViewById(R.id.remark14ET);
                remark15ET = (EditText) dialog.findViewById(R.id.remark15ET);
                remark16ET = (EditText) dialog.findViewById(R.id.remark16ET);
                remark17ET = (EditText) dialog.findViewById(R.id.remark17ET);
                remark18ET = (EditText) dialog.findViewById(R.id.remark18ET);

                remark1TL = (TextInputLayout) dialog.findViewById(R.id.remark1TL);
                remark2TL = (TextInputLayout) dialog.findViewById(R.id.remark2TL);
                remark3TL = (TextInputLayout) dialog.findViewById(R.id.remark3TL);
                remark4TL = (TextInputLayout) dialog.findViewById(R.id.remark4TL);
                remark5TL = (TextInputLayout) dialog.findViewById(R.id.remark5TL);
                remark6TL = (TextInputLayout) dialog.findViewById(R.id.remark6TL);
                remark7TL = (TextInputLayout) dialog.findViewById(R.id.remark7TL);
                remark8TL = (TextInputLayout) dialog.findViewById(R.id.remark8TL);
                remark9TL = (TextInputLayout) dialog.findViewById(R.id.remark9TL);
                remark10TL = (TextInputLayout) dialog.findViewById(R.id.remark10TL);
                remark11TL = (TextInputLayout) dialog.findViewById(R.id.remark11TL);
                remark12TL = (TextInputLayout) dialog.findViewById(R.id.remark12TL);
                remark13TL = (TextInputLayout) dialog.findViewById(R.id.remark13TL);
                remark14TL = (TextInputLayout) dialog.findViewById(R.id.remark14TL);
                remark15TL = (TextInputLayout) dialog.findViewById(R.id.remark15TL);
                remark16TL = (TextInputLayout) dialog.findViewById(R.id.remark16TL);
                remark17TL = (TextInputLayout) dialog.findViewById(R.id.remark17TL);
                remark18TL = (TextInputLayout) dialog.findViewById(R.id.remark18TL);

                insp1 = (MultiStateToggleButton) dialog.findViewById(R.id.insp1);
                insp2 = (MultiStateToggleButton) dialog.findViewById(R.id.insp2);
                insp3 = (MultiStateToggleButton) dialog.findViewById(R.id.insp3);
                insp4 = (MultiStateToggleButton) dialog.findViewById(R.id.insp4);
                insp5 = (MultiStateToggleButton) dialog.findViewById(R.id.insp5);
                insp6 = (MultiStateToggleButton) dialog.findViewById(R.id.insp6);
                insp7 = (MultiStateToggleButton) dialog.findViewById(R.id.insp7);
                insp8 = (MultiStateToggleButton) dialog.findViewById(R.id.insp8);
                insp9 = (MultiStateToggleButton) dialog.findViewById(R.id.insp9);
                insp10 = (MultiStateToggleButton) dialog.findViewById(R.id.insp10);
                insp11 = (MultiStateToggleButton) dialog.findViewById(R.id.insp11);
                insp12 = (MultiStateToggleButton) dialog.findViewById(R.id.insp12);
                insp13 = (MultiStateToggleButton) dialog.findViewById(R.id.insp13);
                insp14 = (MultiStateToggleButton) dialog.findViewById(R.id.insp14);
                insp15 = (MultiStateToggleButton) dialog.findViewById(R.id.insp15);
                insp16 = (MultiStateToggleButton) dialog.findViewById(R.id.insp16);
                insp17 = (MultiStateToggleButton) dialog.findViewById(R.id.insp17);
                insp18 = (MultiStateToggleButton) dialog.findViewById(R.id.insp18);

                Q1TV = (TextView) dialog.findViewById(R.id.Q1TV);
                Q2TV = (TextView) dialog.findViewById(R.id.Q2TV);
                Q3TV = (TextView) dialog.findViewById(R.id.Q3TV);
                Q4TV = (TextView) dialog.findViewById(R.id.Q4TV);
                Q5TV = (TextView) dialog.findViewById(R.id.Q5TV);
                Q6TV = (TextView) dialog.findViewById(R.id.Q6TV);
                Q7TV = (TextView) dialog.findViewById(R.id.Q7TV);
                Q8TV = (TextView) dialog.findViewById(R.id.Q8TV);
                Q9TV = (TextView) dialog.findViewById(R.id.Q9TV);
                Q10TV = (TextView) dialog.findViewById(R.id.Q10TV);
                Q11TV = (TextView) dialog.findViewById(R.id.Q11TV);
                Q12TV = (TextView) dialog.findViewById(R.id.Q12TV);
                Q13TV = (TextView) dialog.findViewById(R.id.Q13TV);
                Q14TV = (TextView) dialog.findViewById(R.id.Q14TV);
                Q15TV = (TextView) dialog.findViewById(R.id.Q15TV);
                Q16TV = (TextView) dialog.findViewById(R.id.Q16TV);
                Q17TV = (TextView) dialog.findViewById(R.id.Q17TV);
                Q18TV = (TextView) dialog.findViewById(R.id.Q18TV);
                Q60TV = (TextView) dialog.findViewById(R.id.Q60TV);
                Q70TV = (TextView) dialog.findViewById(R.id.Q70TV);
                Q80TV = (TextView) dialog.findViewById(R.id.Q80TV);
                Q90TV = (TextView) dialog.findViewById(R.id.Q90TV);
                Q100TV = (TextView) dialog.findViewById(R.id.Q100TV);

                final DataSource dataSource = new DataSource(mActivity);
                dataSource.open();

                SprinklerModel dataComm = dataSource.getDataCommissioning(equipment, date, location, type, register);

                equipmentET.setText(equipment);
                dateET.setText(date);
                locationET.setText(location);
                typeET.setText(type);
                registerET.setText(register);
                clientET.setText(dataComm.getClient());

                if(dataComm.getContractor() == null) {
                    cseTL.setHint("Installer");
                } else {
                    cseTL.setHint(dataComm.getContractor());
                }

                if(dataComm.getDivision() == null) {
                    ugmrTL.setHint("Team Response");
                } else {
                    ugmrTL.setHint(dataComm.getDivision());
                }


                if(dataComm.getQuestion1().toString().equalsIgnoreCase("Pass")) {
                    insp1.setValue(0);
                } else if (dataComm.getQuestion1().toString().equalsIgnoreCase("No")) {
                    insp1.setValue(1);
                } else {
                    insp1.setValue(2);
                }
                remark1ET.setText(dataComm.getRemark1());

                if(dataComm.getQuestion2().toString().equalsIgnoreCase("Pass")) {
                    insp2.setValue(0);
                } else if (dataComm.getQuestion2().toString().equalsIgnoreCase("No")) {
                    insp2.setValue(1);
                } else {
                    insp2.setValue(2);
                }
                remark2ET.setText(dataComm.getRemark2());

                if(dataComm.getQuestion3().toString().equalsIgnoreCase("Pass")) {
                    insp3.setValue(0);
                } else if (dataComm.getQuestion3().toString().equalsIgnoreCase("No")) {
                    insp3.setValue(1);
                } else {
                    insp3.setValue(2);
                }
                remark3ET.setText(dataComm.getRemark3());

                if(dataComm.getQuestion4().toString().equalsIgnoreCase("Pass")) {
                    insp4.setValue(0);
                } else if (dataComm.getQuestion4().toString().equalsIgnoreCase("No")) {
                    insp4.setValue(1);
                } else {
                    insp4.setValue(2);
                }
                remark4ET.setText(dataComm.getRemark4());

                if(dataComm.getQuestion5().toString().equalsIgnoreCase("Pass")) {
                    insp5.setValue(0);
                } else if (dataComm.getQuestion5().toString().equalsIgnoreCase("No")) {
                    insp5.setValue(1);
                } else {
                    insp5.setValue(2);
                }
                remark5ET.setText(dataComm.getRemark5());

                if(dataComm.getQuestion6().toString().equalsIgnoreCase("Pass")) {
                    insp6.setValue(0);
                } else if (dataComm.getQuestion6().toString().equalsIgnoreCase("No")) {
                    insp6.setValue(1);
                } else {
                    insp6.setValue(2);
                }
                remark6ET.setText(dataComm.getRemark6());

                if(dataComm.getQuestion7().toString().equalsIgnoreCase("Pass")) {
                    insp7.setValue(0);
                } else if (dataComm.getQuestion7().toString().equalsIgnoreCase("No")) {
                    insp7.setValue(1);
                } else {
                    insp7.setValue(2);
                }
                remark7ET.setText(dataComm.getRemark7());

                if(dataComm.getQuestion8().toString().equalsIgnoreCase("Pass")) {
                    insp8.setValue(0);
                } else if (dataComm.getQuestion8().toString().equalsIgnoreCase("No")) {
                    insp8.setValue(1);
                } else {
                    insp8.setValue(2);
                }
                remark8ET.setText(dataComm.getRemark8());

                if(dataComm.getQuestion9().toString().equalsIgnoreCase("Pass")) {
                    insp9.setValue(0);
                } else if (dataComm.getQuestion9().toString().equalsIgnoreCase("No")) {
                    insp9.setValue(1);
                } else {
                    insp9.setValue(2);
                }
                remark9ET.setText(dataComm.getRemark9());

                if(dataComm.getQuestion10().toString().equalsIgnoreCase("Pass")) {
                    insp10.setValue(0);
                } else if (dataComm.getQuestion10().toString().equalsIgnoreCase("No")) {
                    insp10.setValue(1);
                } else {
                    insp10.setValue(2);
                }
                remark10ET.setText(dataComm.getRemark10());

                if(dataComm.getQuestion11().toString().equalsIgnoreCase("Pass")) {
                    insp11.setValue(0);
                } else if (dataComm.getQuestion11().toString().equalsIgnoreCase("No")) {
                    insp11.setValue(1);
                } else {
                    insp11.setValue(2);
                }
                remark11ET.setText(dataComm.getRemark11());

                if(dataComm.getQuestion12().toString().equalsIgnoreCase("Pass")) {
                    insp12.setValue(0);
                } else if (dataComm.getQuestion12().toString().equalsIgnoreCase("No")) {
                    insp12.setValue(1);
                } else {
                    insp12.setValue(2);
                }
                remark12ET.setText(dataComm.getRemark12());

                if(dataComm.getQuestion13().toString().equalsIgnoreCase("Pass")) {
                    insp13.setValue(0);
                } else if (dataComm.getQuestion13().toString().equalsIgnoreCase("No")) {
                    insp13.setValue(1);
                } else {
                    insp13.setValue(2);
                }
                remark13ET.setText(dataComm.getRemark13());

                if(dataComm.getQuestion14().toString().equalsIgnoreCase("Pass")) {
                    insp14.setValue(0);
                } else if (dataComm.getQuestion14().toString().equalsIgnoreCase("No")) {
                    insp14.setValue(1);
                } else {
                    insp14.setValue(2);
                }
                remark14ET.setText(dataComm.getRemark14());

                if(dataComm.getQuestion15().toString().equalsIgnoreCase("Pass")) {
                    insp15.setValue(0);
                } else if (dataComm.getQuestion15().toString().equalsIgnoreCase("No")) {
                    insp15.setValue(1);
                } else {
                    insp15.setValue(2);
                }
                remark15ET.setText(dataComm.getRemark15());

                if(dataComm.getQuestion16().toString().equalsIgnoreCase("Pass")) {
                    insp16.setValue(0);
                } else if (dataComm.getQuestion16().toString().equalsIgnoreCase("No")) {
                    insp16.setValue(1);
                } else {
                    insp16.setValue(2);
                }
                remark16ET.setText(dataComm.getRemark16());

                if(dataComm.getQuestion17().toString().equalsIgnoreCase("Pass")) {
                    insp17.setValue(0);
                } else if (dataComm.getQuestion17().toString().equalsIgnoreCase("No")) {
                    insp17.setValue(1);
                } else {
                    insp17.setValue(2);
                }
                remark17ET.setText(dataComm.getRemark17());

                if(dataComm.getQuestion18().toString().equalsIgnoreCase("Pass")) {
                    insp18.setValue(0);
                } else if (dataComm.getQuestion18().toString().equalsIgnoreCase("No")) {
                    insp18.setValue(1);
                } else {
                    insp18.setValue(2);
                }
                remark18ET.setText(dataComm.getRemark18());

                engineeringET.setText(dataComm.getNameEng());
                maintenanceET.setText(dataComm.getNameMain());
                areaOwnerET.setText(dataComm.getNameAO());
                ugmrET.setText(dataComm.getNameUGMR());
                cseET.setText(dataComm.getNameCSE());
                deptHeadET.setText(dataComm.getNameDept());

                activeFModel = new PhotoModel();

                addComplianceIV = (ImageView) dialog.findViewById(R.id.addComplianceIV);
                addComplianceIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopUp.showCompliance(mActivity, equipmentET.getText().toString(), dateET.getText().toString(), registerET.getText().toString(),
                                null, null, null, null, true, new PopUpComplianceCallback() {
                                    @Override
                                    public void onDataSave(ComplianceModel model) {
                                        DataSource ds = new DataSource(mActivity);
                                        ds.open();

                                        ds.insertCompliance(model);

                                        layoutManager = new LinearLayoutManager(mActivity);
                                        findingsRV.setLayoutManager(layoutManager);
                                        findingsRV.setItemAnimator(new DefaultItemAnimator());

                                        complianceData = ds.getAllComplianceEquipment(equipmentET.getText().toString(),
                                                dateET.getText().toString(), registerET.getText().toString());

                                        adapterData = new ListComplianceAdapter(mActivity, complianceData);
                                        findingsRV.setAdapter(adapterData);

                                        ds.close();
                                    }
                                });
                    }
                });

                addPhotosIV = (ImageView) dialog.findViewById(R.id.addPhotosIV);

                addPhotosIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PhotoPopUp.showAddImagePopUp(mActivity, equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString(), new PopUpPhotosCallback() {
                                    @Override
                                    public void onDataSave(PhotoModel model) {
                                        DataSource ds = new DataSource(mActivity);
                                        ds.open();

                                        ds.insertPhotos(model);

                                        photosLayoutManager = new LinearLayoutManager(mActivity);
                                        photosRV.setLayoutManager(photosLayoutManager);
                                        photosRV.setItemAnimator(new DefaultItemAnimator());

                                        photoData = ds.getAllPhotoData(equipmentET.getText().toString(),
                                                dateET.getText().toString(), registerET.getText().toString());

                                        adapterPhotosData = new ListPhotoAdapter(mActivity, photoData);
                                        photosRV.setAdapter(adapterPhotosData);

                                        ds.close();
                                    }
                                });
                    }
                });

                addPhotosIV = (ImageView) dialog.findViewById(R.id.addPhotosIV);

                findingsLL = (LinearLayout) dialog.findViewById(R.id.findingsLL);
                photosLL = (LinearLayout) dialog.findViewById(R.id.photosLL);

                findingsRV = (RecyclerView) dialog.findViewById(R.id.findingsRV);
                photosRV = (RecyclerView) dialog.findViewById(R.id.photosRV);

                complianceData = dataSource.getAllComplianceEquipment(equipmentET.getText().toString(),
                        dateET.getText().toString(), registerET.getText().toString());

                if (complianceData.size() > 0) {
//                    layoutManager = new LinearLayoutManager(mActivity);
                    findingsRV.setLayoutManager(new LinearLayoutManager(mActivity));
                    findingsRV.setItemAnimator(new DefaultItemAnimator());

                    adapterData = new ListComplianceAdapter(mActivity, complianceData);
                    findingsRV.setAdapter(adapterData);
                }

                photoData = dataSource.getAllPhotoData(equipmentET.getText().toString(),
                        dateET.getText().toString(), registerET.getText().toString());

                if (photoData.size() > 0) {
//                    photosLayoutManager = new LinearLayoutManager(mActivity);
                    photosRV.setLayoutManager(new LinearLayoutManager(mActivity));
                    photosRV.setItemAnimator(new DefaultItemAnimator());

                    adapterPhotosData = new ListPhotoAdapter(mActivity, photoData);
                    photosRV.setAdapter(adapterPhotosData);
                }

                dataSource.close();

                generateCV = (CardView) dialog.findViewById(R.id.generateCV);
                previewCV = (CardView) dialog.findViewById(R.id.previewCV);

                generateCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(validation()) {
                            Reports.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), true);
                        }
                    }
                });

                previewCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(validation()) {
                            Reports.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), false);
                        }
                    }
                });

                Q80TV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(findingsRV.getVisibility() == View.GONE) {
                            findingsRV.setVisibility(View.VISIBLE);
                        } else if ( findingsRV.getVisibility() == View.VISIBLE) {
                            findingsRV.setVisibility(View.GONE);
                        }
                    }
                });

                Q90TV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(photosRV.getVisibility() == View.GONE) {
                            photosRV.setVisibility(View.VISIBLE);
                        } else if ( photosRV.getVisibility() == View.VISIBLE) {
                            photosRV.setVisibility(View.GONE);
                        }
                    }
                });

//                clientET.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (validation()) {
//                            DataSource ds = new DataSource(mActivity);
//                            ds.open();
//                            if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                saveData(mActivity);
//                                signatures = PopUp.signature.client;
//                                scanBar(signatures, mActivity);
//                            } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                updateData(mActivity);
//                                signatures = PopUp.signature.client;
//                                scanBar(signatures, mActivity);
//                            }
//                            ds.close();
//                        }
//                    }
//                });
//
//                engineeringET.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (validation()) {
//                            DataSource ds = new DataSource(mActivity);
//                            ds.open();
//                            if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                saveData(mActivity);
//                                signatures = PopUp.signature.engineering;
//                                scanBar(signatures, mActivity);
//                            } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                updateData(mActivity);
//                                signatures = PopUp.signature.engineering;
//                                scanBar(signatures, mActivity);
//                            }
//                            ds.close();
//                        }
//                    }
//                });
//
                maintenanceET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validation()) {
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                saveData(mActivity);
                                pages = PopUp.page.commissioning;
                                signatures = PopUp.signature.maintenance;
                                scanBar(signatures, pages, mActivity);
                            } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                updateData(mActivity);
                                signatures = PopUp.signature.maintenance;
                                scanBar(signatures, pages, mActivity);
                            }
                            ds.close();
                        }
                    }
                });

                areaOwnerET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validation()) {
                            final DataSource ds = new DataSource(mActivity);
                            ds.open();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.areaOwner;
                            PopUp.showInspectorPopUp(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {
                                            ds.updateAOCommissioning(model);
                                            areaOwnerET.setText(name);
                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                            dataSource.close();
                        }
                    }
                });

                ugmrET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validation()) {
                            final  DataSource ds = new DataSource(mActivity);
                            ds.open();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.mainRes;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {
                                            dataSource.updateMainResCommissioning(model);
                                            ugmrTL.setHint(division);
                                            ugmrET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                            dataSource.close();
                        }
                    }
                });

                cseET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validation()) {
                            final  DataSource ds = new DataSource(mActivity);
                            ds.open();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.CSE;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {
                                            dataSource.updateCSECommissioning(model);
                                            cseTL.setHint(division);
                                            cseET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                            dataSource.close();
                        }
                    }
                });

                deptHeadET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validation()) {
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                saveData(mActivity);
                                signatures = PopUp.signature.deptHead;
                                pages = PopUp.page.commissioning;
                                scanBar(signatures, pages, mActivity);
                            } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                updateData(mActivity);
                                signatures = PopUp.signature.deptHead;
                                scanBar(signatures, pages, mActivity);
                            }
                            ds.close();
                        }
                    }
                });

                final Calendar c = Calendar.getInstance();
                selectedYear = c.get(Calendar.YEAR);
                selectedMonth = c.get(Calendar.MONTH);
                selectedDate = c.get(Calendar.DAY_OF_MONTH);
                DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                        selectedDate);
                dateET.setText(DataSingleton.getInstance().getFormattedDate());

                languageToggleButton = (android.widget.ToggleButton) dialog.findViewById(R.id.toggleBtnLanguange);

                compilanceBtns = new MultiStateToggleButton[]{insp1, insp2, insp3, insp4, insp5, insp6, insp7, insp8, insp9, insp10, insp11,
                        insp12, insp13, insp14, insp15, insp16, insp17, insp18};

                compilanceLastValue = new int[]{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                        -1, -1, -1, -1, -1, -1};

                isChangedLanguage = false;
                dialog.show();
            }
        });
    }

    public void setLanguage(boolean isEnglish) {
        if (isEnglish) {
            languageConstants = new LanguageConstants(false);
        } else {
            languageConstants = new LanguageConstants(true);
        }

        Q1TV.setText(languageConstants.Q1);
        Q2TV.setText(languageConstants.Q2);
        Q3TV.setText(languageConstants.Q3);
        Q4TV.setText(languageConstants.Q4);
        Q5TV.setText(languageConstants.Q5);
        Q6TV.setText(languageConstants.Q6);
        Q7TV.setText(languageConstants.Q7);
        Q8TV.setText(languageConstants.Q8);
        Q9TV.setText(languageConstants.Q9);
        Q10TV.setText(languageConstants.Q10);
        Q11TV.setText(languageConstants.Q11);
        Q12TV.setText(languageConstants.Q12);
        Q13TV.setText(languageConstants.Q13);
        Q14TV.setText(languageConstants.Q14);
        Q15TV.setText(languageConstants.Q15);
        Q16TV.setText(languageConstants.Q16);
        Q17TV.setText(languageConstants.Q17);
        Q18TV.setText(languageConstants.Q18);

        isChangedLanguage = true;

        for (MultiStateToggleButton compilanceBtn : compilanceBtns) {
            compilanceBtn.setValue(-1);
            // With an array
            CharSequence[] textEn = new CharSequence[]{"Pass", "No", "N/A"};
            CharSequence[] textId = new CharSequence[]{"Ya", "Tidak", "N/A"};
            if (isEnglish)
                compilanceBtn.setElements(textId);
            else
                compilanceBtn.setElements(textEn);
        }

        isChangedLanguage = false;

        int index = 0;
        for (MultiStateToggleButton compilanceBtn : compilanceBtns) {
            compilanceBtn.setValue(compilanceLastValue[index]);
            index++;
        }
    }

    public static void datePickers(final Activity mActivity, final View rootView) {
        Helper.showDatePicker(rootView, (FragmentActivity) mActivity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        selectedYear = year;
                        selectedMonth = monthOfYear;
                        selectedDate = dayOfMonth;

                        DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                        ((EditText) rootView).setText(DataSingleton.getInstance()
                                .getFormattedDate());
                    }
                }, selectedYear, selectedMonth, selectedDate);
    }

    public static boolean validation() {
        boolean status = true;

        if (equipmentET.getText().toString().length() == 0) {
            status = false;
            equipmentTL.setErrorEnabled(true);
            equipmentTL.setError("*Please fill Sprinkler Equipment");
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill Date Inspection");
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill Sprinkler Location");
        }

        if (typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("*Please fill Sprinkler Type");
        }

//        if (registerET.getText().toString().length() == 0) {
//            status = false;
//            registerTL.setErrorEnabled(true);
//            registerTL.setError("*Please fill Sprinkler Register");
//        }

        if (insp1.getValue() == 1){
            if (remark1ET.getText().toString().length() == 0) {
                status = false;
                remark1TL.setErrorEnabled(true);
                remark1TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark1ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp1.getValue() == -1) {
            status = false;
            remark1TL.setErrorEnabled(true);
            remark1TL.setError("*Please fill Inspection Choice");
        }

        if (insp2.getValue() == 1){
            if (remark2ET.getText().toString().length() == 0) {
                status = false;
                remark2TL.setErrorEnabled(true);
                remark2TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark2ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp2.getValue() == -1) {
            status = false;
            remark2TL.setErrorEnabled(true);
            remark2TL.setError("*Please fill Inspection Choice");
        }

        if (insp3.getValue() == 1){
            if (remark3ET.getText().toString().length() == 0) {
                status = false;
                remark3TL.setErrorEnabled(true);
                remark3TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark3ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp3.getValue() == -1) {
            status = false;
            remark3TL.setErrorEnabled(true);
            remark3TL.setError("*Please fill Inspection Choice");
        }

        if (insp4.getValue() == 1){
            if (remark4ET.getText().toString().length() == 0) {
                status = false;
                remark4TL.setErrorEnabled(true);
                remark4TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark4ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp4.getValue() == -1) {
            status = false;
            remark4TL.setErrorEnabled(true);
            remark4TL.setError("*Please fill Inspection Choice");
        }

        if (insp5.getValue() == 1){
            if (remark5ET.getText().toString().length() == 0) {
                status = false;
                remark5TL.setErrorEnabled(true);
                remark5TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark5ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp5.getValue() == -1) {
            status = false;
            remark5TL.setErrorEnabled(true);
            remark5TL.setError("*Please fill Inspection Choice");
        }

        if (insp6.getValue() == 1){
            if (remark6ET.getText().toString().length() == 0) {
                status = false;
                remark6TL.setErrorEnabled(true);
                remark6TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark6ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp6.getValue() == -1) {
            status = false;
            remark6TL.setErrorEnabled(true);
            remark6TL.setError("*Please fill Inspection Choice");
        }

        if (insp7.getValue() == 1){
            if (remark7ET.getText().toString().length() == 0) {
                status = false;
                remark7TL.setErrorEnabled(true);
                remark7TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark7ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp7.getValue() == -1) {
            status = false;
            remark7TL.setErrorEnabled(true);
            remark7TL.setError("*Please fill Inspection Choice");
        }

        if (insp8.getValue() == 1){
            if (remark8ET.getText().toString().length() == 0) {
                status = false;
                remark8TL.setErrorEnabled(true);
                remark8TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark8ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp8.getValue() == -1) {
            status = false;
            remark8TL.setErrorEnabled(true);
            remark8TL.setError("*Please fill Inspection Choice");
        }

        if (insp9.getValue() == 1){
            if (remark9ET.getText().toString().length() == 0) {
                status = false;
                remark9TL.setErrorEnabled(true);
                remark9TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark9ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp9.getValue() == -1) {
            status = false;
            remark9TL.setErrorEnabled(true);
            remark9TL.setError("*Please fill Inspection Choice");
        }

        if (insp10.getValue() == 1){
            if (remark10ET.getText().toString().length() == 0) {
                status = false;
                remark10TL.setErrorEnabled(true);
                remark10TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark10ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp10.getValue() == -1) {
            status = false;
            remark10TL.setErrorEnabled(true);
            remark10TL.setError("*Please fill Inspection Choice");
        }

        if (insp11.getValue() == 1){
            if (remark11ET.getText().toString().length() == 0) {
                status = false;
                remark11TL.setErrorEnabled(true);
                remark11TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark11ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp11.getValue() == -1) {
            status = false;
            remark11TL.setErrorEnabled(true);
            remark11TL.setError("*Please fill Inspection Choice");
        }

        if (insp12.getValue() == 1){
            if (remark12ET.getText().toString().length() == 0) {
                status = false;
                remark12TL.setErrorEnabled(true);
                remark12TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark1ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp12.getValue() == -1) {
            status = false;
            remark12TL.setErrorEnabled(true);
            remark12TL.setError("*Please fill Inspection Choice");
        }

        if (insp13.getValue() == 1){
            if (remark13ET.getText().toString().length() == 0) {
                status = false;
                remark13TL.setErrorEnabled(true);
                remark13TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark13ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp13.getValue() == -1) {
            status = false;
            remark13TL.setErrorEnabled(true);
            remark13TL.setError("*Please fill Inspection Choice");
        }

        if (insp14.getValue() == 1){
            if (remark14ET.getText().toString().length() == 0) {
                status = false;
                remark14TL.setErrorEnabled(true);
                remark14TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark14ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp14.getValue() == -1) {
            status = false;
            remark14TL.setErrorEnabled(true);
            remark14TL.setError("*Please fill Inspection Choice");
        }

        if (insp15.getValue() == 1){
            if (remark15ET.getText().toString().length() == 0) {
                status = false;
                remark15TL.setErrorEnabled(true);
                remark15TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark15ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp15.getValue() == -1) {
            status = false;
            remark15TL.setErrorEnabled(true);
            remark15TL.setError("*Please fill Inspection Choice");
        }

        if (insp16.getValue() == 1){
            if (remark16ET.getText().toString().length() == 0) {
                status = false;
                remark16TL.setErrorEnabled(true);
                remark16TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark16ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp16.getValue() == -1) {
            status = false;
            remark16TL.setErrorEnabled(true);
            remark16TL.setError("*Please fill Inspection Choice");
        }

        if (insp17.getValue() == 1){
            if (remark17ET.getText().toString().length() == 0) {
                status = false;
                remark17TL.setErrorEnabled(true);
                remark17TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark17ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp17.getValue() == -1) {
            status = false;
            remark17TL.setErrorEnabled(true);
            remark17TL.setError("*Please fill Inspection Choice");
        }

        if (insp18.getValue() == 1){
            if (remark18ET.getText().toString().length() == 0) {
                status = false;
                remark18TL.setErrorEnabled(true);
                remark18TL.setError("*Please fill remark if you choose 'No'");
            } else if (remark18ET.getText().toString().length() > 0) {
                status = true;
            }
        } else if (insp18.getValue() == -1) {
            status = false;
            remark18TL.setErrorEnabled(true);
            remark18TL.setError("*Please fill Inspection Choice");
        }

        return status;
    }

    public static void saveData(final Activity mActivity) {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        SprinklerModel model = new SprinklerModel();

        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());
        if (insp1.getValue() == 0) {
            model.setQuestion1("Pass");
        } else if (insp1.getValue() == 1) {
            model.setQuestion1("No");
        } else {
            model.setQuestion1("N/A");
        }
        if (insp2.getValue() == 0) {
            model.setQuestion2("Pass");
        } else if (insp2.getValue() == 1) {
            model.setQuestion2("No");
        } else {
            model.setQuestion2("N/A");
        }
        if (insp3.getValue() == 0) {
            model.setQuestion3("Pass");
        } else if (insp3.getValue() == 1) {
            model.setQuestion3("No");
        } else {
            model.setQuestion3("N/A");
        }
        if (insp4.getValue() == 0) {
            model.setQuestion4("Pass");
        } else if (insp4.getValue() == 1) {
            model.setQuestion4("No");
        } else {
            model.setQuestion4("N/A");
        }
        if (insp5.getValue() == 0) {
            model.setQuestion5("Pass");
        } else if (insp5.getValue() == 1) {
            model.setQuestion5("No");
        } else {
            model.setQuestion5("N/A");
        }
        if (insp6.getValue() == 0) {
            model.setQuestion6("Pass");
        } else if (insp6.getValue() == 1) {
            model.setQuestion6("No");
        } else {
            model.setQuestion6("N/A");
        }
        if (insp7.getValue() == 0) {
            model.setQuestion7("Pass");
        } else if (insp7.getValue() == 1) {
            model.setQuestion7("No");
        } else {
            model.setQuestion7("N/A");
        }
        if (insp8.getValue() == 0) {
            model.setQuestion8("Pass");
        } else if (insp8.getValue() == 1) {
            model.setQuestion8("No");
        } else {
            model.setQuestion8("N/A");
        }
        if (insp9.getValue() == 0) {
            model.setQuestion9("Pass");
        } else if (insp9.getValue() == 1) {
            model.setQuestion9("No");
        } else {
            model.setQuestion9("N/A");
        }
        if (insp10.getValue() == 0) {
            model.setQuestion10("Pass");
        } else if (insp10.getValue() == 1) {
            model.setQuestion10("No");
        } else {
            model.setQuestion10("N/A");
        }
        if (insp11.getValue() == 0) {
            model.setQuestion11("Pass");
        } else if (insp11.getValue() == 1) {
            model.setQuestion11("No");
        } else {
            model.setQuestion11("N/A");
        }
        if (insp12.getValue() == 0) {
            model.setQuestion12("Pass");
        } else if (insp12.getValue() == 1) {
            model.setQuestion12("No");
        } else {
            model.setQuestion12("N/A");
        }
        if (insp13.getValue() == 0) {
            model.setQuestion13("Pass");
        } else if (insp13.getValue() == 1) {
            model.setQuestion13("No");
        } else {
            model.setQuestion13("N/A");
        }
        if (insp14.getValue() == 0) {
            model.setQuestion14("Pass");
        } else if (insp14.getValue() == 1) {
            model.setQuestion14("No");
        } else {
            model.setQuestion14("N/A");
        }
        if (insp15.getValue() == 0) {
            model.setQuestion15("Pass");
        } else if (insp15.getValue() == 1) {
            model.setQuestion15("No");
        } else {
            model.setQuestion15("N/A");
        }
        if (insp16.getValue() == 0) {
            model.setQuestion16("Pass");
        } else if (insp16.getValue() == 1) {
            model.setQuestion16("No");
        } else {
            model.setQuestion16("N/A");
        }
        if (insp17.getValue() == 0) {
            model.setQuestion17("Pass");
        } else if (insp17.getValue() == 1) {
            model.setQuestion17("No");
        } else {
            model.setQuestion17("N/A");
        }
        if (insp18.getValue() == 0) {
            model.setQuestion18("Pass");
        } else if (insp18.getValue() == 1) {
            model.setQuestion18("No");
        } else {
            model.setQuestion18("N/A");
        }
        model.setRemark1(remark1ET.getText().toString());
        model.setRemark2(remark2ET.getText().toString());
        model.setRemark3(remark3ET.getText().toString());
        model.setRemark4(remark4ET.getText().toString());
        model.setRemark5(remark5ET.getText().toString());
        model.setRemark6(remark6ET.getText().toString());
        model.setRemark7(remark7ET.getText().toString());
        model.setRemark8(remark8ET.getText().toString());
        model.setRemark9(remark9ET.getText().toString());
        model.setRemark10(remark10ET.getText().toString());
        model.setRemark11(remark11ET.getText().toString());
        model.setRemark12(remark12ET.getText().toString());
        model.setRemark13(remark13ET.getText().toString());
        model.setRemark14(remark14ET.getText().toString());
        model.setRemark15(remark15ET.getText().toString());
        model.setRemark16(remark16ET.getText().toString());
        model.setRemark17(remark17ET.getText().toString());
        model.setRemark18(remark18ET.getText().toString());

        ds.insertCommissioning(model);
        ds.close();
    }

    public static void updateData(final Activity mActivity) {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        SprinklerModel model = new SprinklerModel();

        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());
        if (insp1.getValue() == 0) {
            model.setQuestion1("Pass");
        } else if (insp1.getValue() == 1) {
            model.setQuestion1("No");
        } else {
            model.setQuestion1("N/A");
        }
        if (insp2.getValue() == 0) {
            model.setQuestion2("Pass");
        } else if (insp2.getValue() == 1) {
            model.setQuestion2("No");
        } else {
            model.setQuestion2("N/A");
        }
        if (insp3.getValue() == 0) {
            model.setQuestion3("Pass");
        } else if (insp3.getValue() == 1) {
            model.setQuestion3("No");
        } else {
            model.setQuestion3("N/A");
        }
        if (insp4.getValue() == 0) {
            model.setQuestion4("Pass");
        } else if (insp4.getValue() == 1) {
            model.setQuestion4("No");
        } else {
            model.setQuestion4("N/A");
        }
        if (insp5.getValue() == 0) {
            model.setQuestion5("Pass");
        } else if (insp5.getValue() == 1) {
            model.setQuestion5("No");
        } else {
            model.setQuestion5("N/A");
        }
        if (insp6.getValue() == 0) {
            model.setQuestion6("Pass");
        } else if (insp6.getValue() == 1) {
            model.setQuestion6("No");
        } else {
            model.setQuestion6("N/A");
        }
        if (insp7.getValue() == 0) {
            model.setQuestion7("Pass");
        } else if (insp7.getValue() == 1) {
            model.setQuestion7("No");
        } else {
            model.setQuestion7("N/A");
        }
        if (insp8.getValue() == 0) {
            model.setQuestion8("Pass");
        } else if (insp8.getValue() == 1) {
            model.setQuestion8("No");
        } else {
            model.setQuestion8("N/A");
        }
        if (insp9.getValue() == 0) {
            model.setQuestion9("Pass");
        } else if (insp9.getValue() == 1) {
            model.setQuestion9("No");
        } else {
            model.setQuestion9("N/A");
        }
        if (insp10.getValue() == 0) {
            model.setQuestion10("Pass");
        } else if (insp10.getValue() == 1) {
            model.setQuestion10("No");
        } else {
            model.setQuestion10("N/A");
        }
        if (insp11.getValue() == 0) {
            model.setQuestion11("Pass");
        } else if (insp11.getValue() == 1) {
            model.setQuestion11("No");
        } else {
            model.setQuestion11("N/A");
        }
        if (insp12.getValue() == 0) {
            model.setQuestion12("Pass");
        } else if (insp12.getValue() == 1) {
            model.setQuestion12("No");
        } else {
            model.setQuestion12("N/A");
        }
        if (insp13.getValue() == 0) {
            model.setQuestion13("Pass");
        } else if (insp13.getValue() == 1) {
            model.setQuestion13("No");
        } else {
            model.setQuestion13("N/A");
        }
        if (insp14.getValue() == 0) {
            model.setQuestion14("Pass");
        } else if (insp14.getValue() == 1) {
            model.setQuestion14("No");
        } else {
            model.setQuestion14("N/A");
        }
        if (insp15.getValue() == 0) {
            model.setQuestion15("Pass");
        } else if (insp15.getValue() == 1) {
            model.setQuestion15("No");
        } else {
            model.setQuestion15("N/A");
        }
        if (insp16.getValue() == 0) {
            model.setQuestion16("Pass");
        } else if (insp16.getValue() == 1) {
            model.setQuestion16("No");
        } else {
            model.setQuestion16("N/A");
        }
        if (insp17.getValue() == 0) {
            model.setQuestion17("Pass");
        } else if (insp17.getValue() == 1) {
            model.setQuestion17("No");
        } else {
            model.setQuestion17("N/A");
        }
        if (insp18.getValue() == 0) {
            model.setQuestion18("Pass");
        } else if (insp18.getValue() == 1) {
            model.setQuestion18("No");
        } else {
            model.setQuestion18("N/A");
        }
        model.setRemark1(remark1ET.getText().toString());
        model.setRemark2(remark2ET.getText().toString());
        model.setRemark3(remark3ET.getText().toString());
        model.setRemark4(remark4ET.getText().toString());
        model.setRemark5(remark5ET.getText().toString());
        model.setRemark6(remark6ET.getText().toString());
        model.setRemark7(remark7ET.getText().toString());
        model.setRemark8(remark8ET.getText().toString());
        model.setRemark9(remark9ET.getText().toString());
        model.setRemark10(remark10ET.getText().toString());
        model.setRemark11(remark11ET.getText().toString());
        model.setRemark12(remark12ET.getText().toString());
        model.setRemark13(remark13ET.getText().toString());
        model.setRemark14(remark14ET.getText().toString());
        model.setRemark15(remark15ET.getText().toString());
        model.setRemark16(remark16ET.getText().toString());
        model.setRemark17(remark17ET.getText().toString());
        model.setRemark18(remark18ET.getText().toString());

        ds.updateCommissioningData(model);
        ds.close();
    }

    private static android.app.AlertDialog showQRDialog(final Activity act,
                                                        CharSequence title, CharSequence message, CharSequence buttonYes,
                                                        CharSequence buttonNo) {
        android.app.AlertDialog.Builder downloadDialog = new android.app.AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Helper.copyAndOpenBarcodeScannerAPK(act);
                    }
                });
        downloadDialog.setNegativeButton(buttonNo,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        return downloadDialog.show();
    }

    public static void scanBar(PopUp.signature signatures, PopUp.page pages, final Activity mActivity) {
        try {
            Intent intent = new Intent(mActivity, CaptureActivity.class);
            intent.setAction(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "ONE_D_MODE");
            switch (signatures) {
//                case engineering:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_ENG);
//                    break;

                case maintenance:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAIN);
                    break;

//                case areaOwner:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_AO);
//                    break;
//
//                case mainRes:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAINRES);
//                    break;
//
//                case CSE:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_CSE);
//                    break;

                case deptHead:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_DEPT);
                    break;

                default:
                    break;
            }
        } catch (ActivityNotFoundException anfe) {
            showQRDialog(mActivity, "No Scanner Found",
                    "Install a scanner code application?", "Yes", "No").show();
        }
    }

    public static void handOverPopUp (final Activity mActivity, final String equipment, final String date,
                                      final String location, final String type, final String register) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_handover);

                equipmentET = (EditText) dialog.findViewById(R.id.equipmentET);
                registerET = (EditText) dialog.findViewById(R.id.registerET);
                dateET = (EditText) dialog.findViewById(R.id.dateET);
                clientET = (EditText) dialog.findViewById(R.id.clientET);
                typeET = (EditText) dialog.findViewById(R.id.typeEqET);
                locationET = (EditText) dialog.findViewById(R.id.locationEqET);
                addTermsET = (EditText) dialog.findViewById(R.id.addTermsET);

                equipmentTL = (TextInputLayout) dialog.findViewById(R.id.equipmentTL);
                registerTL = (TextInputLayout) dialog.findViewById(R.id.registerTL);
                dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);
                clientTL = (TextInputLayout) dialog.findViewById(R.id.clientTL);
                typeTL = (TextInputLayout) dialog.findViewById(R.id.typeEqTL);
                locationTL = (TextInputLayout) dialog.findViewById(R.id.locationEqTL);
                addTermsTL = (TextInputLayout) dialog.findViewById(R.id.addTermsTL);

                engineeringET = (EditText) dialog.findViewById(R.id.engineeringET);
                maintenanceET = (EditText) dialog.findViewById(R.id.maintenanceET);
                areaOwnerET = (EditText) dialog.findViewById(R.id.areaOwnerET);
                ugmrET = (EditText) dialog.findViewById(R.id.ugmrET);
                cseET = (EditText) dialog.findViewById(R.id.cseET);
                deptHeadET = (EditText) dialog.findViewById(R.id.deptHeadET);

                engineeringTL = (TextInputLayout) dialog.findViewById(R.id.engineeringTL);
                maintenanceTL = (TextInputLayout) dialog.findViewById(R.id.maintenanceTL);
                areaOwnerTL = (TextInputLayout) dialog.findViewById(R.id.areaOwnerTL);
                ugmrTL = (TextInputLayout) dialog.findViewById(R.id.ugmrTL);
                cseTL = (TextInputLayout) dialog.findViewById(R.id.cseTL);
                deptHeadTL = (TextInputLayout) dialog.findViewById(R.id.deptHeadTL);

                termsLL = (LinearLayout) dialog.findViewById(R.id.termsLL);

                termsRV = (RecyclerView) dialog.findViewById(R.id.termsRV);

                DataSource dataSource = new DataSource(mActivity);
                dataSource.open();

                HandoverModel hoData = new HandoverModel();
                hoData = dataSource.getDataHandOver(equipment,date, location, type, register);

                equipmentET.setText(equipment);
                dateET.setText(date);
                locationET.setText(location);
                typeET.setText(type);
                registerET.setText(register);
                clientET.setText(hoData.getClient());

                if(hoData.getContractor() == null) {
                    cseTL.setHint("Installer");
                } else {
                    cseTL.setHint(hoData.getContractor());
                }

                if(hoData.getDivision() == null) {
                    ugmrTL.setHint("Team Response");
                } else {
                    ugmrTL.setHint(hoData.getDivision());
                }


                termsData = dataSource.getAllTermsEquipment(equipmentET.getText().toString(),
                        dateET.getText().toString(), registerET.getText().toString());

                if (termsData.size() > 0) {
                    termsRV.setHasFixedSize(true);
                    layoutManager = new LinearLayoutManager(mActivity);
                    termsRV.setLayoutManager(layoutManager);
                    termsRV.setItemAnimator(new DefaultItemAnimator());

                    adapterTermData = new ListTermsAdapter(mActivity, termsData);
                    termsRV.setAdapter(adapterTermData);
                }

                engineeringET.setText(hoData.getNameEng());
                maintenanceET.setText(hoData.getNameMain());
                areaOwnerET.setText(hoData.getNameAO());
                ugmrET.setText(hoData.getNameUGMR());
                cseET.setText(hoData.getNameCSE());
                deptHeadET.setText(hoData.getNameDept());

                addCV = (CardView) dialog.findViewById(R.id.addCV);
                addCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (addTermsET.getText().toString().length() >= 1) {
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            TermsModel model = new TermsModel();

                            model.setTerms(addTermsET.getText().toString());
                            model.setEquipment(equipmentET.getText().toString());
                            model.setDate(dateET.getText().toString());
                            model.setRegister(registerET.getText().toString());

                            ds.insertTerms(model);

                            termsRV.setHasFixedSize(true);
                            layoutManager = new LinearLayoutManager(mActivity);
                            termsRV.setLayoutManager(layoutManager);
                            termsRV.setItemAnimator(new DefaultItemAnimator());

                            termsData = ds.getAllTermsEquipment(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString());

                            adapterTermData = new ListTermsAdapter(mActivity, termsData);
                            termsRV.setAdapter(adapterData);

                            ds.close();

                            addTermsET.setText("");
                        } else if (addTermsET.getText().toString().length() == 0) {
                            addTermsTL.setErrorEnabled(true);
                            addTermsTL.setError("*Please fill this Terms and Conditions");
                        }
                    }
                });
                generateCV = (CardView) dialog.findViewById(R.id.generateCV);
                previewCV = (CardView) dialog.findViewById(R.id.previewCV);

                generateCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Reports.createHandOverReport(mActivity, equipment, date, location, type, register, true);
                    }
                });

                previewCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Reports.createHandOverReport(mActivity, equipment, date, location, type, register, false);
                    }
                });

//                clientET.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (validation()) {
//                            DataSource ds = new DataSource(mActivity);
//                            ds.open();
//                            if (!ds.validateDataHandOver(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                saveDataHandOver(mActivity);
//                                signatures = PopUp.signature.client;
//                                scanBarHO(signatures,mActivity);
//                            } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                updateDataHandOver(mActivity);
//                                signatures = PopUp.signature.client;
//                                scanBarHO(signatures, mActivity);
//                            }
//                            ds.close();
//                        }
//                    }
//                });
//
//                engineeringET.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (validation()) {
//                            DataSource ds = new DataSource(mActivity);
//                            ds.open();
//                            if (!ds.validateDataHandOver(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                saveDataHandOver(mActivity);
//                                signatures = PopUp.signature.engineering;
//                                pages = PopUp.page.handover;
//                                scanBarHO(signatures, pages, mActivity);
//                            } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                updateDataHandOver(mActivity);
//                                signatures = PopUp.signature.engineering;
//                                pages = PopUp.page.handover;
//                                scanBarHO(signatures, pages, mActivity);
//                            }
//                            ds.close();
//                        }
//                    }
//                });

                maintenanceET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                saveDataHandOver(mActivity);
                                signatures = PopUp.signature.maintenance;
                                pages = PopUp.page.handover;
                                scanBarHO(signatures, pages, mActivity);
                            } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                updateDataHandOver(mActivity);
                                signatures = PopUp.signature.maintenance;
                                pages = PopUp.page.handover;
                                scanBarHO(signatures, pages, mActivity);
                            }
                            ds.close();
                    }
                });

                areaOwnerET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            final  DataSource ds = new DataSource(mActivity);
                            ds.open();
                            PopUp.page pages = PopUp.page.handover;
                            PopUp.signature signatures = PopUp.signature.areaOwner;
                            PopUp.showInspectorPopUp(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {
                                            ds.updateAOHandOver(model);
                                            areaOwnerET.setText(name);

                                        }
                                    });
                            ds.close();
                    }
                });
//
                ugmrET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            final DataSource ds = new DataSource(mActivity);
                            ds.open();
                            PopUp.page pages = PopUp.page.handover;
                            PopUp.signature signatures = PopUp.signature.mainRes;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {
                                            ds.updateMainResHandOver(model);
                                            ugmrTL.setHint(division);
                                            ugmrET.setText(name);
                                        }
                                    });
                            ds.close();
                    }
                });

                cseET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                           final  DataSource ds = new DataSource(mActivity);
                            ds.open();
                            PopUp.page pages = PopUp.page.handover;
                            PopUp.signature signatures = PopUp.signature.CSE;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(SprinklerModel model, String name, String division) {

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {
                                            ds.updateCSEHandOver(model);
                                            cseTL.setHint(division);
                                            cseET.setText(name);
                                        }
                                    });
                            ds.close();
                    }
                });

                deptHeadET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                saveDataHandOver(mActivity);
                                signatures = PopUp.signature.deptHead;
                                pages = PopUp.page.handover;
                                scanBarHO(signatures, pages, mActivity);
                            } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                updateDataHandOver(mActivity);
                                signatures = PopUp.signature.deptHead;
                                pages = PopUp.page.handover;
                                scanBarHO(signatures, pages, mActivity);
                            }
                            ds.close();
                    }
                });

                // Date
                final Calendar c = Calendar.getInstance();
                selectedYear = c.get(Calendar.YEAR);
                selectedMonth = c.get(Calendar.MONTH);
                selectedDate = c.get(Calendar.DAY_OF_MONTH);
                DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                        selectedDate);
                dateET.setText(DataSingleton.getInstance().getFormattedDate());

                dialog.show();
            }
        });
    }

    public static void saveDataHandOver(final Activity mActivity) {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        HandoverModel model = new HandoverModel();
        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());

        ds.insertHandOver(model);

        ds.close();
    }

    public static void updateDataHandOver(final Activity mActivity) {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        HandoverModel model = new HandoverModel();
        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());

        ds.updateHandOverData(model);

        ds.close();
    }

    public static void scanBarHO(PopUp.signature signatures, PopUp.page pages, final Activity mActivity) {
        try {
            Intent intent = new Intent(mActivity, CaptureActivity.class);
            intent.setAction(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "ONE_D_MODE");
            switch (signatures) {
//                case engineering:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_ENG_HO);
//                    break;

                case maintenance:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAIN_HO);
                    break;

//                case areaOwner:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_AO_HO);
//                    break;
//
//                case mainRes:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAINRES_HO);
//                    break;
//
//                case CSE:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_CSE_HO);
//                    break;

                case deptHead:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_DEPT_HO);
                    break;

                default:
                    break;
            }
        } catch (ActivityNotFoundException anfe) {
            showQRDialog(mActivity, "No Scanner Found",
                    "Install a scanner code application?", "Yes", "No").show();
        }
    }
}
