package com.ptfi.commsprinkler.PopUp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptfi.commsprinkler.Database.DataSource;
import com.ptfi.commsprinkler.Models.ComplianceModel;
import com.ptfi.commsprinkler.Models.HandoverModel;
import com.ptfi.commsprinkler.Models.SprinklerModel;
import com.ptfi.commsprinkler.R;
import com.ptfi.commsprinkler.Utils.Helper;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

/**
 * Created by senaardyputra on 6/16/16.
 */
public class PopUp {

    static final int REQUEST_SCAN_BARCODE_AO = 350;
    static final int REQUEST_SCAN_BARCODE_MAINRES = 351;
    static final int REQUEST_SCAN_BARCODE_CSE = 352;
    static final int REQUEST_SCAN_BARCODE_CLIENT = 347;

    public enum page {
        commissioning, handover, generateData, manageData
    }

    public static page pageComm;

    public enum signature {
        engineering, maintenance, areaOwner, mainRes, CSE, deptHead
    }

    static EditText divisionET, idET, nameET;
    static TextInputLayout divisionTL, idTL, nameTL;

    public static boolean validationInspector() {
        boolean status = true;

        if (idET.getText().toString().length() == 0) {
            status = false;
            idTL.setErrorEnabled(true);
            idTL.setError("Please fill inspector ID");
        }

        if (nameET.getText().toString().length() == 0) {
            status = false;
            nameTL.setErrorEnabled(true);
            nameTL.setError("Please fill inspector Name");
        }

        return status;
    }

    public static void showInspectorPopUp (final Activity mActivity, final String id, final String name,
                                    final page pages, final signature signatures, final String equipment, final String date,
                                           final String register, final PopUpCallbackInspector callback) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_inspector);


                final LinearLayout signature, imageWrapper;
                final ImageView imageSignature;
                final TextView signatureWarning;
                final CardView submitCV, cancelCV;

                idET = (EditText) dialog.findViewById(R.id.idET);
                nameET = (EditText) dialog.findViewById(R.id.nameET);

                idTL = (TextInputLayout) dialog.findViewById(R.id.idTL);
                nameTL = (TextInputLayout) dialog.findViewById(R.id.nameTL);

                signature = (LinearLayout) dialog.findViewById(R.id.signature);
                imageWrapper = (LinearLayout) dialog.findViewById(R.id.imageWrapper);
                imageSignature = (ImageView) dialog.findViewById(R.id.signatureImage);

                signatureWarning = (TextView) dialog.findViewById(R.id.signatureWarning);

                submitCV = (CardView) dialog.findViewById(R.id.submitCV);
                cancelCV = (CardView) dialog.findViewById(R.id.cancelCV);

                idET.setText(id);
                nameET.setText(name);

                signature.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GesturePopUp.showGesture(mActivity, new GestureCallback() {

                                @Override
                                public void onBitmapSaved(Bitmap bitmap) {
                                    if (bitmap != null) {
                                        imageSignature
                                                .setImageBitmap(bitmap);
                                        signatureWarning
                                                .setVisibility(View.GONE);
                                    } else {
                                        signatureWarning
                                                .setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                    }
                });

                submitCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(validationInspector()) {
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            final SprinklerModel model = new SprinklerModel();
                            final HandoverModel modelHO = new HandoverModel();
                            switch (pages) {
                                case commissioning:
                                    switch (signatures) {
                                        case engineering:
                                            model.setEquipment(equipment);
                                            model.setDate(date);
                                            model.setRegister(register);
                                            model.setNameEng(nameET.getText().toString());
                                            model.setIdEng(idET.getText().toString());
                                            model.setSignEng(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));

                                            ds.updateEngCommissioning(model);
                                            break;

                                        case maintenance:
                                            model.setEquipment(equipment);
                                            model.setDate(date);
                                            model.setRegister(register);
                                            model.setNameMain(nameET.getText().toString());
                                            model.setIdMain(idET.getText().toString());
                                            model.setSignMain(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));

                                            ds.updateMainCommissioning(model);
                                            break;

                                        case areaOwner:
                                            model.setEquipment(equipment);
                                            model.setDate(date);
                                            model.setRegister(register);
                                            model.setNameAO(nameET.getText().toString());
                                            model.setIdAO(idET.getText().toString());
                                            model.setSignAO(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));

                                            callback.onDataSave(model, nameET.getText().toString(), null);
                                            break;

                                        case deptHead:
                                            model.setEquipment(equipment);
                                            model.setDate(date);
                                            model.setRegister(register);
                                            model.setNameDept(nameET.getText().toString());
                                            model.setIdDept(idET.getText().toString());
                                            model.setSignDept(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));

                                            ds.updateDeptCommissioning(model);
                                            break;

                                        default:
                                            break;
                                    }
                                    break;

                                case handover:
                                    switch (signatures) {
                                        case engineering:
                                            modelHO.setEquipment(equipment);
                                            modelHO.setDate(date);
                                            modelHO.setRegister(register);
                                            modelHO.setNameEng(nameET.getText().toString());
                                            modelHO.setIdEng(idET.getText().toString());
                                            modelHO.setSignEng(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));

                                            ds.updateEngHandOver(modelHO);
                                            break;

                                        case maintenance:
                                            modelHO.setEquipment(equipment);
                                            modelHO.setDate(date);
                                            modelHO.setRegister(register);
                                            modelHO.setNameMain(nameET.getText().toString());
                                            modelHO.setIdMain(idET.getText().toString());
                                            modelHO.setSignMain(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));

                                            ds.updateMainHandOver(modelHO);
                                            break;

                                        case areaOwner:
                                            modelHO.setEquipment(equipment);
                                            modelHO.setDate(date);
                                            modelHO.setRegister(register);
                                            modelHO.setNameAO(nameET.getText().toString());
                                            modelHO.setIdAO(idET.getText().toString());
                                            modelHO.setSignAO(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));

                                            callback.onDataSaveHO(modelHO, nameET.getText().toString(), null);
                                            break;

                                        case deptHead:
                                            modelHO.setEquipment(equipment);
                                            modelHO.setDate(date);
                                            modelHO.setRegister(register);
                                            modelHO.setNameDept(nameET.getText().toString());
                                            modelHO.setIdDept(idET.getText().toString());
                                            modelHO.setSignDept(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));

                                            ds.updateDeptHandOver(modelHO);
                                            break;

                                        default:
                                            break;
                                    }
                                    break;

                                default:
                                    break;
                            }

                            ds.close();

                            dialog.dismiss();
                        }
                    }
                });

                cancelCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
    }

    public static boolean validationDivision() {
        boolean status = true;

        if (divisionET.getText().toString().length() == 0) {
            status = false;
            divisionTL.setErrorEnabled(true);
            divisionTL.setError("Please fill division / contractor");
        }

        if (idET.getText().toString().length() == 0) {
            status = false;
            idTL.setErrorEnabled(true);
            idTL.setError("Please fill inspector ID");
        }

        if (nameET.getText().toString().length() == 0) {
            status = false;
            nameTL.setErrorEnabled(true);
            nameTL.setError("Please fill inspector Name");
        }

        return status;
    }

    public static void showInspectorPopUpDivision (final Activity mActivity, final String id, final String name,
                                           final page pages, final signature signatures, final String equipment, final String date,
                                           final String register, final PopUpCallbackInspector callback) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_inspector_division);


                final LinearLayout signature, imageWrapper;
                final ImageView imageSignature;
                final TextView signatureWarning;
                final CardView submitCV, cancelCV;

                divisionET = (EditText) dialog.findViewById(R.id.divisionET);
                idET = (EditText) dialog.findViewById(R.id.idET);
                nameET = (EditText) dialog.findViewById(R.id.nameET);

                divisionTL = (TextInputLayout) dialog.findViewById(R.id.divisionTL);
                idTL = (TextInputLayout) dialog.findViewById(R.id.idTL);
                nameTL = (TextInputLayout) dialog.findViewById(R.id.nameTL);

                signature = (LinearLayout) dialog.findViewById(R.id.signature);
                imageWrapper = (LinearLayout) dialog.findViewById(R.id.imageWrapper);
                imageSignature = (ImageView) dialog.findViewById(R.id.signatureImage);

                signatureWarning = (TextView) dialog.findViewById(R.id.signatureWarning);

                submitCV = (CardView) dialog.findViewById(R.id.submitCV);
                cancelCV = (CardView) dialog.findViewById(R.id.cancelCV);

                idET.setText(id);
                nameET.setText(name);

                signature.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GesturePopUp.showGesture(mActivity, new GestureCallback() {

                            @Override
                            public void onBitmapSaved(Bitmap bitmap) {
                                if (bitmap != null) {
                                    imageSignature
                                            .setImageBitmap(bitmap);
                                    signatureWarning
                                            .setVisibility(View.GONE);
                                } else {
                                    signatureWarning
                                            .setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                });

                submitCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(validationDivision()) {
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            final SprinklerModel model = new SprinklerModel();
                            final HandoverModel modelHO = new HandoverModel();
                            switch (pages) {
                                case commissioning:
                                    switch (signatures) {
                                        case mainRes:
                                            model.setEquipment(equipment);
                                            model.setDate(date);
                                            model.setRegister(register);
                                            model.setNameUGMR(nameET.getText().toString());
                                            model.setIdUGMR(idET.getText().toString());
                                            model.setSignUGMR(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));
                                            model.setDivision(divisionET.getText().toString());

                                            callback.onDataSave(model, nameET.getText().toString(), divisionET.getText().toString());
                                            break;

                                        case CSE:
                                            model.setEquipment(equipment);
                                            model.setDate(date);
                                            model.setRegister(register);
                                            model.setNameCSE(nameET.getText().toString());
                                            model.setIdCSE(idET.getText().toString());
                                            model.setSignCSE(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));
                                            model.setContractor(divisionET.getText().toString());

                                            callback.onDataSave(model, nameET.getText().toString(), divisionET.getText().toString());
                                            break;

                                        default:
                                            break;
                                    }
                                    break;

                                case handover:
                                    switch (signatures) {
                                        case mainRes:
                                            modelHO.setEquipment(equipment);
                                            modelHO.setDate(date);
                                            modelHO.setRegister(register);
                                            modelHO.setNameUGMR(nameET.getText().toString());
                                            modelHO.setIdUGMR(idET.getText().toString());
                                            modelHO.setSignUGMR(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));
                                            modelHO.setDivision(divisionET.getText().toString());

                                            callback.onDataSaveHO(modelHO, nameET.getText().toString(), divisionET.getText().toString());
                                            break;

                                        case CSE:
                                            modelHO.setEquipment(equipment);
                                            modelHO.setDate(date);
                                            modelHO.setRegister(register);
                                            modelHO.setNameCSE(nameET.getText().toString());
                                            modelHO.setIdCSE(idET.getText().toString());
                                            modelHO.setSignCSE(Helper.bitmapToByte(((BitmapDrawable) imageSignature
                                                    .getDrawable()) != null ? ((BitmapDrawable) imageSignature
                                                    .getDrawable()).getBitmap() : null));
                                            modelHO.setContractor(divisionET.getText().toString());

                                            callback.onDataSaveHO(modelHO, nameET.getText().toString(), divisionET.getText().toString());
                                            break;

                                        default:
                                            break;
                                    }
                                    break;

                                default:
                                    break;
                            }

                            ds.close();

                            dialog.dismiss();
                        }
                    }
                });

                cancelCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
    }

    static EditText findingsET, remarkET, responsibilityET;
    static TextInputLayout findingsTL, remarkTL, responsibilityTL;
    static MultiStateToggleButton status;

    public static boolean validationCompliance() {
        boolean statusValid = true;

        if (findingsET.getText().toString().length() == 0) {
            statusValid = false;
            findingsTL.setErrorEnabled(true);
            findingsTL.setError("Please fill finding first");
        }

        if (remarkET.getText().toString().length() == 0) {
            statusValid = false;
            remarkTL.setErrorEnabled(true);
            remarkTL.setError("Please fill remarks first");
        }

        if (responsibilityET.getText().toString().length() == 0) {
            statusValid = false;
            responsibilityTL.setErrorEnabled(true);
            responsibilityTL.setError("Please fill responsibility first");
        }

        if(status.getValue() == -1) {
            statusValid = false;
            responsibilityTL.setErrorEnabled(true);
            responsibilityTL.setError("Please choose 1 of status choice");
        }

        return statusValid;
    }

    public static void showCompliance (final Activity mActivity, final String equipment, final String date, final String register,
                                       final String findings, final String remark, final String respons, final String done,
                                       final boolean isNew, final PopUpComplianceCallback callback) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_compliance);

                final CardView cancelCV, submitCV;

                findingsET = (EditText) dialog.findViewById(R.id.findingsET);
                remarkET = (EditText) dialog.findViewById(R.id.remarkET);
                responsibilityET = (EditText) dialog.findViewById(R.id.responsibilityET);

                findingsTL = (TextInputLayout) dialog.findViewById(R.id.findingsTL);
                remarkTL = (TextInputLayout) dialog.findViewById(R.id.remarkTL);
                responsibilityTL = (TextInputLayout) dialog.findViewById(R.id.responsibilityTL);

                status = (MultiStateToggleButton) dialog.findViewById(R.id.status);

                cancelCV = (CardView) dialog.findViewById(R.id.cancelCV);
                submitCV = (CardView) dialog.findViewById(R.id.submitCV);

                if (!isNew) {
                    findingsET.setText(findings);
                    remarkET.setText(remark);
                    responsibilityET.setText(respons);
                    if (done.equals("Done")) {
                        status.setValue(0);
                    } else {
                        status.setValue(1);
                    }
                }

                submitCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validationCompliance()) {
                            ComplianceModel model = new ComplianceModel();
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            model.setFindings(findingsET.getText().toString());
                            model.setRemark(remarkET.getText().toString());
                            model.setResponsibility(responsibilityET.getText().toString());
                            model.setEquipment(equipment);
                            model.setDate(date);
                            model.setRegister(register);
                            if (status.getValue() == 0) {
                                model.setDone("Done");
                            } else {
                                model.setDone("Not Done");
                            }

                            callback.onDataSave(model);

                            dialog.dismiss();
                        }
                    }
                });

                cancelCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
    }

}
