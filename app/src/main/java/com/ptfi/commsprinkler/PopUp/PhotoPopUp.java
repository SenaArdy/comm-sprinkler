package com.ptfi.commsprinkler.PopUp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptfi.commsprinkler.Database.DataSource;
import com.ptfi.commsprinkler.Models.PhotoModel;
import com.ptfi.commsprinkler.R;
import com.ptfi.commsprinkler.Utils.FoldersFilesName;
import com.ptfi.commsprinkler.Utils.Helper;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by senaardyputra on 6/20/16.
 */
public class PhotoPopUp {

    static ImageView picture;
    public static final int IMAGE_POP_UP_TAKE_PICTURE_CODE = 24;
    public static final int IMAGE_POP_UP_GALLERY_CODE = 25;

    private static String imagePath = null;
    private static String deletedImagePath = null;
    private static InputStream originalImage= null;
    private static String temporaryDir = null;

    static EditText titleET, commentET;
    static TextInputLayout titleTL, commentTL;

    public static boolean validationPhoto() {
        boolean status = true;

        if (titleET.getText().toString().length() == 0) {
            status = false;
            titleTL.setErrorEnabled(true);
            titleTL.setError("Please fill title first");
        }

        if (commentET.getText().toString().length() == 0) {
            status = false;
            commentTL.setErrorEnabled(true);
            commentTL.setError("Please fill comment first");
        }

        return status;
    }

    public static void showAddImagePopUp(final Activity mActivity, final String equipment,
                                         final String date, final String register, final PopUpPhotosCallback callback) {
        if (mActivity != null) {
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    final Dialog dialog = new Dialog(mActivity);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.popup_photo);
                    dialog.setCanceledOnTouchOutside(false);

                    final CardView submitCV, cancelCV;

                    titleET = (EditText) dialog.findViewById(R.id.titleET);
                    commentET = (EditText) dialog.findViewById(R.id.commentET);

                    titleTL = (TextInputLayout) dialog.findViewById(R.id.titleTL);
                    commentTL = (TextInputLayout) dialog.findViewById(R.id.commentTL);

                    picture = (ImageView) dialog.findViewById(R.id.picture);

                    submitCV = (CardView) dialog.findViewById(R.id.submitCV);
                    cancelCV = (CardView) dialog.findViewById(R.id.cancelCV);

                    picture.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            chooseMedia(mActivity);
                        }
                    });

                    submitCV.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (validationPhoto()) {
                                PhotoModel insertFoto = new PhotoModel();
                                DataSource ds = new DataSource(mActivity);
                                ds.open();
                                insertFoto.setPathFile(imagePath);
                                insertFoto.setTitle(titleET.getText().toString());
                                insertFoto.setComment(commentET.getText().toString());
                                insertFoto.setEquipment(equipment);
                                insertFoto.setDate(date);
                                insertFoto.setRegister(register);

                                callback.onDataSave(insertFoto);
                                ds.close();
                                dialog.dismiss();
                            }
                        }
                    });

                    cancelCV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
            });
        }
    }

    public static void showImagePopUp(final Activity mActivity, final String equipment,
                                      final String date, final String register, final String filePath,
                                      final String title, final String comment) {
        if (mActivity != null) {
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    final Dialog dialog = new Dialog(mActivity);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.popup_photo);
                    dialog.setCanceledOnTouchOutside(false);

                    final TextInputLayout titleTL, commentTL;
                    final CardView submitCV, cancelCV;

                    titleET = (EditText) dialog.findViewById(R.id.titleET);
                    commentET = (EditText) dialog.findViewById(R.id.commentET);

                    titleTL = (TextInputLayout) dialog.findViewById(R.id.titleTL);
                    commentTL = (TextInputLayout) dialog.findViewById(R.id.commentTL);

                    picture = (ImageView) dialog.findViewById(R.id.picture);

                    submitCV = (CardView) dialog.findViewById(R.id.submitCV);
                    cancelCV = (CardView) dialog.findViewById(R.id.cancelCV);

                    titleET.setText(title);
                    commentET.setText(comment);
                    Helper.setPic(picture, filePath);

                    picture.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            chooseMedia(mActivity);
                        }
                    });

                    submitCV.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (validationPhoto()) {
                                PhotoModel insertFoto = new PhotoModel();
                                DataSource ds = new DataSource(mActivity);
                                ds.open();
                                if (ds.validatePhotoData(equipment, date, register, filePath, title, comment)) {
                                    insertFoto.setPathFile(imagePath);
                                    insertFoto.setTitle(titleET.getText().toString());
                                    insertFoto.setComment(commentET.getText().toString());

                                    ds.updatePhotoData(insertFoto, filePath, title, comment);
                                }

                                ds.close();
                                dialog.dismiss();
                            }
                        }
                    });

                    cancelCV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
            });

        }
    }

    public static void chooseMedia (final Activity mActivity) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_choose_foto);
                dialog.setCanceledOnTouchOutside(false);

                final CardView cameraCV, galleryCV;

                cameraCV = (CardView) dialog.findViewById(R.id.cameraCV);
                galleryCV = (CardView) dialog.findViewById(R.id.galleryCV);

                cameraCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tempDir = FoldersFilesName.tempPhotosDir;
                        String currentTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

                        File exportFolder = new File(tempDir);
                        if (!exportFolder.exists()) {
                            //create specific folder for taken image unless it's already created
                            exportFolder.mkdirs();
                        }

                        String photoName = "Commissioning Sprinkler" + currentTime + ".jpg";

                        Intent intent = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);

                        Helper.fileUri = Uri.fromFile(new File(tempDir,
                                photoName));

                        intent.putExtra(
                                MediaStore.EXTRA_OUTPUT,
                                Helper.fileUri);
                        mActivity.startActivityForResult(intent,
                                IMAGE_POP_UP_TAKE_PICTURE_CODE);

                        dialog.dismiss();
                    }
                });

                galleryCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        mActivity.startActivityForResult(intent,
                                IMAGE_POP_UP_GALLERY_CODE);

                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    public static void showPopupPreview(final Activity activity,
                                        final String filePath) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_preview_image);

                TextView title = (TextView) dialog.findViewById(R.id.title);
                ImageView iv = (ImageView) dialog
                        .findViewById(R.id.imagePreview);

                Helper.setPic(iv, filePath);

                title.setText(filePath.substring(filePath.lastIndexOf("/") + 1));

                CardView closeBtn = (CardView) dialog
                        .findViewById(R.id.cancelCV);
                closeBtn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    public static ImageView getPicture() {
        return picture;
    }

    public static void setImagePath(String imagePath) {
        PhotoPopUp.imagePath = imagePath;
    }
}
