package com.ptfi.commsprinkler.PopUp;

import com.ptfi.commsprinkler.Models.HandoverModel;
import com.ptfi.commsprinkler.Models.PhotoModel;
import com.ptfi.commsprinkler.Models.SprinklerModel;

/**
 * Created by senaardyputra on 7/27/16.
 */
public interface PopUpCallbackInspector {
    public void onDataSave(SprinklerModel model, String name, String division);

    public void onDataSaveHO(HandoverModel model, String name, String division);
}
