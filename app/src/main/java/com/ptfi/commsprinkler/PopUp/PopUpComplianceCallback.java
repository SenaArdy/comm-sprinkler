package com.ptfi.commsprinkler.PopUp;

import com.ptfi.commsprinkler.Models.ComplianceModel;

/**
 * Created by senaardyputra on 6/18/16.
 */
public interface PopUpComplianceCallback {
    public void onDataSave(ComplianceModel model);
}
