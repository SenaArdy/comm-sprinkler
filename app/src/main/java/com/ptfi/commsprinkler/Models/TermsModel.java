package com.ptfi.commsprinkler.Models;

/**
 * Created by senaardyputra on 6/16/16.
 */
public class TermsModel {

    public long id;
    public String terms;
    public String equipment;
    public String date;
    public String register;

    public TermsModel() {
        this.id = -1;
        this.terms = "";
        this.equipment = "";
        this.date = "";
        this.register = "";
    }

    public TermsModel(long id, String terms, String equipment, String date, String register) {
        this.id = id;
        this.terms = terms;
        this.equipment = equipment;
        this.date = date;
        this.register = register;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }
}
